import Vue from 'vue'
import config from '../../config'

const BACKGROUNDS_LOADED = 'BACKGROUNDS_LOADED'
const OBJECTS_LOADED = 'OBJECTS_LOADED'
const MODELS_LOADED = 'MODELS_LOADED'
const UPLOADS_LOADED = 'UPLOADS_LOADED'

const SEARCH_BACKGROUNDS_SET = 'SEARCH_BACKGROUNDS_SET'
const SEARCH_MODELS_SET = 'SEARCH_MODELS_SET'
const SEARCH_OBJECTS_SET = 'SEARCH_OBJECTS_SET'

const RELATED_BACKGROUNDS_SET = 'RELATED_BACKGROUNDS_SET'
const RELATED_MODELS_SET = 'RELATED_MODELS_SET'
const RELATED_OBJECTS_SET = 'RELATED_OBJECTS_SET'
const RELATED_TO_ITEM_ITEMS_SET = 'RELATED_TO_ITEM_ITEMS_SET'
const RELATED_TO_STAGE_ITEMS_SET = 'RELATED_TO_STAGE_ITEMS_SET'
const RELATED_TO_STAGE_ITEMS_LOADING = 'RELATED_TO_STAGE_ITEMS_LOADING'
const RELATED_TO_STAGE_ITEMS_LOADED = 'RELATED_TO_STAGE_ITEMS_LOADED'
const RELATED_ITEMS_LIST_SHOWN = 'RELATED_ITEMS_LIST_SHOWN'

const UNDO_STACK_SIZE_SET = 'UNDO_STACK_SIZE_SET'
const REDO_STACK_SIZE_SET = 'REDO_STACK_SIZE_SET'

const USER_COLLAGE_SET = 'USER_COLLAGE_SET'

const STAGE_UPDATED = 'STAGE_UPDATED'

const USER_SET = 'USER_SET'
const USER_COLLAGES_SET = 'USER_COLLAGES_SET'
const USER_COLLAGE_UPDATED = 'USER_COLLAGE_UPDATED'

const FONT_LOADED = 'FONT_LOADED'
const LEFT_SIDEBAR_WIDTH_SET = 'LEFT_SIDEBAR_WIDTH_SET'
const SEARCH_QUERY_SET = 'SEARCH_QUERY_SET'
const RIGHT_SIDEBAR_SHOWN = 'RIGHT_SIDEBAR_SHOWN'
const ELEMENT_SELECTED = 'ELEMENT_SELECTED'
const LEFT_SIDEBAR_EXPANDED = 'LEFT_SIDEBAR_EXPANDED'
const ERRORS_SET = 'ERRORS_SET'
const CLEAR_STORE = 'CLEAR_STORE'
const LICENSE_VERIFIED = 'LICENSE_VERIFIED'
const STAGE_SIZES_SHOWN = 'STAGE_SIZES_SHOWN'
const CURRENT_ACTION = 'CURRENT_ACTION'
const IS_MOBILE = 'IS_MOBILE'
const IS_PORTRAIT = 'IS_PORTRAIT'
const AUTOSAVE_STATUS_SET = 'AUTOSAVE_STATUS_SET'

const ADD_UPLOAD = 'ADD_UPLOAD'
const UPDATE_UPLOAD = 'UPDATE_UPLOAD'
const DELETE_UPLOAD = 'DELETE_UPLOAD'
const FACE_SELECTED = 'FACE_SELECTED'

const state = () => ({
  isMobile: false,
  isPortrait: false,
  user: null,
  stage: { showOffset: true },
  leftSidebarWidth: config.leftSidebar.minWidth,
  leftSidebarExpanded: true,
  autosave: {
    timer: null,
    status: null
  },
  currentAction: null,
  faceSelected: false,
  backgrounds: {
    items: [],
    total: 0,
    sorting: null,
    page: 0
  },
  models: {
    items: [],
    total: 0,
    sorting: null,
    page: 0
  },
  objects: {
    items: [],
    total: 0,
    sorting: null,
    page: 0
  },
  uploads: {
    items: [],
    total: 0,
    page: 0
  },
  currentUploads: [],
  searchBackgrounds: {
    items: [],
    total: 0,
    sorting: null,
    page: 0
  },
  searchModels: {
    items: [],
    total: 0,
    sorting: null,
    page: 0
  },
  searchObjects: {
    items: [],
    total: 0,
    sorting: null,
    page: 0
  },
  userCollages: {
    items: [],
    total: 0,
    page: 0
  },
  stageSizesShown: false,
  related: {
    backgrounds: {
      items: [],
      total: 0,
      page: 0
    },
    models: {
      items: [],
      total: 0,
      page: 0
    },
    objects: {
      items: [],
      total: 0,
      page: 0
    },
    toItemItems: {
      items: [],
      total: 0,
      page: 0
    },
    toStageItems: {
      items: [],
      loading: false
    },
    itemsListShown: true
  },
  isDefaultBackgroundActive: false,
  activeItem: null,
  loadedFonts: [],
  undoStackSize: 0,
  redoStackSize: 0,
  searchQuery: null,
  rightSidebarShown: false,
  userCollage: null,
  licenseVerified: false,
  errors: {
    api: null,
    fatal: null,
    files: {
      incorrectType: [],
      incorrectSize: []
    }
  },
  itemsTotal: {
    models: 0,
    objects: 0,
    backgrounds: 0,
    search: 0,
    userCollages: 0
  }
})

const getters = {
  items: state => state.items
}

const mutations = {
  setDefaultBackgroundActive (state, isDefaultBackgroundActive) {
    state.isDefaultBackgroundActive = isDefaultBackgroundActive
  },
  setAutosaveTimer (state, autosaveTimer) {
    state.autosave.timer = autosaveTimer
  },
  [CLEAR_STORE] (state) {
    state.searchQuery = null
    state.userCollage = null
    state.activeItem = null
    state.searchObjects = { items: [], total: 0, page: 0 }
    state.searchModels = { items: [], total: 0, page: 0 }
    state.searchBackgrounds = { items: [], total: 0, page: 0 }
    state.related.objects = { items: [], total: 0, page: 0 }
    state.related.models = { items: [], total: 0, page: 0 }
    state.related.backgrounds = { items: [], total: 0, page: 0 }
  },
  [RIGHT_SIDEBAR_SHOWN] (state, isRightSidebarShown) {
    state.rightSidebarShown = isRightSidebarShown
  },
  [SEARCH_QUERY_SET] (state, searchQuery) {
    state.searchQuery = searchQuery
  },
  [IS_MOBILE] (state, isMobile) {
    state.isMobile = isMobile
  },
  [IS_PORTRAIT] (state, isPortrait) {
    state.isPortrait = isPortrait
  },
  [AUTOSAVE_STATUS_SET] (state, autosaveStatus) {
    state.autosave.status = autosaveStatus
  },
  [LICENSE_VERIFIED] (state, isLicenseVerified) {
    state.licenseVerified = isLicenseVerified
  },
  [FACE_SELECTED] (state, faceSelected) {
    state.faceSelected = faceSelected
  },
  [RELATED_ITEMS_LIST_SHOWN] (state, isRelatedItemsListShown) {
    state.related.itemsListShown = isRelatedItemsListShown
  },
  [RELATED_TO_STAGE_ITEMS_LOADING] (state) {
    state.related.toStageItems.loading = true
  },
  [RELATED_TO_STAGE_ITEMS_LOADED] (state, relatedToStageItems) {
    state.related.toStageItems.loading = false
  },
  [RELATED_TO_STAGE_ITEMS_SET] (state, relatedToStageItems) {
    state.related.toStageItems.items = relatedToStageItems
  },
  [RELATED_TO_ITEM_ITEMS_SET] (state, { items, total, page }) {
    page = page !== undefined ? page : state.related.toItemItems.page + 1
    state.related.toItemItems = { items, total, page }
  },
  [MODELS_LOADED] (state, { items, total, page, sorting }) {
    page = page !== undefined ? page : state.models.page + 1
    sorting = sorting !== undefined ? sorting : state.models.sorting
    state.models = { items, total, page, sorting }
  },
  [BACKGROUNDS_LOADED] (state, { items, total, page, sorting }) {
    page = page !== undefined ? page : state.backgrounds.page + 1
    sorting = sorting !== undefined ? sorting : state.backgrounds.sorting
    state.backgrounds = { items, total, page, sorting }
  },
  [OBJECTS_LOADED] (state, { items, total, page, sorting }) {
    page = page !== undefined ? page : state.objects.page + 1
    sorting = sorting !== undefined ? sorting : state.objects.sorting
    state.objects = { items, total, page, sorting }
  },
  [UPLOADS_LOADED] (state, { items, total, page }) {
    page = page !== undefined ? page : state.uploads.page + 1
    state.uploads = { items, total, page }
  },
  [USER_COLLAGES_SET] (state, payload) {
    payload.page = payload.page !== undefined ? payload.page : state.objects.page + 1
    Object.assign(state.userCollages, payload)
  },
  [ELEMENT_SELECTED] (state, element) {
    state.activeItem = element
  },
  [CURRENT_ACTION] (state, currentAction) {
    state.currentAction = currentAction
  },
  [ERRORS_SET] (state, errors) {
    state.errors = Object.assign(state.errors, errors)
  },
  [USER_SET] (state, user) {
    state.user = user
  },
  [USER_COLLAGE_SET] (state, userCollage) {
    state.userCollage = userCollage
  },
  [USER_COLLAGE_UPDATED] (state, collageData) {
    let collage = state.userCollages.items.find(collage => collage.id === collageData.id)
    Object.assign(collage, collageData)
  },
  [SEARCH_MODELS_SET] (state, { items, total, page, sorting }) {
    page = page !== undefined ? page : state.searchModels.page + 1
    sorting = sorting !== undefined ? sorting : state.searchModels.sorting
    state.searchModels = { items, total, page, sorting }
  },
  [SEARCH_OBJECTS_SET] (state, { items, total, page, sorting }) {
    page = page !== undefined ? page : state.searchObjects.page + 1
    sorting = sorting !== undefined ? sorting : state.searchObjects.sorting
    state.searchObjects = { items, total, page, sorting }
  },
  [SEARCH_BACKGROUNDS_SET] (state, { items, total, page, sorting }) {
    page = page !== undefined ? page : state.searchBackgrounds.page + 1
    sorting = sorting !== undefined ? sorting : state.searchBackgrounds.sorting
    state.searchBackgrounds = { items, total, page, sorting }
  },
  [RELATED_MODELS_SET] (state, { items, total, page }) {
    page = page !== undefined ? page : state.related.models.page + 1
    state.related.models = { items, total, page }
  },
  [RELATED_OBJECTS_SET] (state, { items, total, page }) {
    page = page !== undefined ? page : state.related.objects.page + 1
    state.related.objects = { items, total, page }
  },
  [RELATED_BACKGROUNDS_SET] (state, { items, total, page }) {
    page = page !== undefined ? page : state.related.backgrounds.page + 1
    state.related.backgrounds = { items, total, page }
  },
  [FONT_LOADED] (state, fontTitle) {
    state.loadedFonts.push(fontTitle)
    state.loadedFonts.sort(function (previousFont, nextFont) { return previousFont.id - nextFont.id })
  },
  [STAGE_UPDATED] (state, stageData) {
    state.stage = Object.assign({}, state.stage, stageData)
  },
  [LEFT_SIDEBAR_WIDTH_SET] (state, leftSidebarWidth) {
    state.leftSidebarWidth = leftSidebarWidth
  },
  [LEFT_SIDEBAR_EXPANDED] (state, isLeftSidebarExpanded) {
    state.leftSidebarExpanded = isLeftSidebarExpanded
  },
  [UNDO_STACK_SIZE_SET] (state, undoStackSize) {
    state.undoStackSize = undoStackSize
  },
  [REDO_STACK_SIZE_SET] (state, redoStackSize) {
    state.redoStackSize = redoStackSize
  },
  [STAGE_SIZES_SHOWN] (state, stageSizesShown) {
    state.stageSizesShown = stageSizesShown
  },
  [ADD_UPLOAD] (state, upload) {
    state.currentUploads.push(upload)
  },
  [UPDATE_UPLOAD] (state, { uploadingId, payload }) {
    let idx = state.currentUploads.findIndex(x => x.uploadingId === uploadingId)
    Vue.set(state.currentUploads, idx, { ...state.currentUploads[idx], ...payload })
  },
  [DELETE_UPLOAD] (state, uploadId) {
    state.currentUploads = state.currentUploads.filter(upload => upload.id !== uploadId)
  }
}

const actions = {
  setBackgrounds ({ commit }, backgrounds) {
    return new Promise(function (resolve) {
      commit(BACKGROUNDS_LOADED, backgrounds)
      resolve()
    })
  },
  setObjects ({ commit }, objects) {
    return new Promise(function (resolve) {
      commit(OBJECTS_LOADED, objects)
      resolve()
    })
  },
  setRelatedToItemItems ({ commit }, items) {
    return new Promise(function (resolve) {
      commit(RELATED_TO_ITEM_ITEMS_SET, items)
      resolve()
    })
  },
  setModels ({ commit }, models) {
    return new Promise(function (resolve) {
      commit(MODELS_LOADED, models)
      resolve()
    })
  },
  setUploads ({ commit }, uploads) {
    return new Promise(function (resolve) {
      commit(UPLOADS_LOADED, uploads)
      resolve()
    })
  },
  elementSelected ({ commit }, element) {
    return new Promise(function (resolve) {
      commit(ELEMENT_SELECTED, element)
      resolve()
    })
  },
  fontLoaded ({ commit }, fontTitle) {
    return new Promise(function (resolve) {
      commit(FONT_LOADED, fontTitle)
      resolve()
    })
  },
  stageUpdated ({ commit }, stageData) {
    return new Promise(function (resolve) {
      commit(STAGE_UPDATED, stageData)
      resolve()
    })
  },
  leftSidebarWidthSet ({ commit }, leftSidebarWidth) {
    return new Promise(function (resolve) {
      commit(LEFT_SIDEBAR_WIDTH_SET, leftSidebarWidth)
      resolve()
    })
  },
  leftSidebarExpandedSet ({ commit }, isLeftSidebarExpanded) {
    return new Promise(function (resolve) {
      commit(LEFT_SIDEBAR_EXPANDED, isLeftSidebarExpanded)
      resolve()
    })
  },
  undoStackSizeSet ({ commit }, undoStackSize) {
    return new Promise(function (resolve) {
      commit(UNDO_STACK_SIZE_SET, undoStackSize)
      resolve()
    })
  },
  redoStackSizeSet ({ commit }, redoStackSize) {
    return new Promise(function (resolve) {
      commit(REDO_STACK_SIZE_SET, redoStackSize)
      resolve()
    })
  },
  searchQuerySet ({ commit }, searchQuery) {
    return new Promise(function (resolve) {
      commit(SEARCH_QUERY_SET, searchQuery)
      resolve()
    })
  },
  userSet ({ commit }, user) {
    return new Promise(function (resolve) {
      commit(USER_SET, user)
      resolve()
    })
  },
  userCollageSet ({ commit }, userCollage) {
    return new Promise(function (resolve) {
      commit(USER_COLLAGE_SET, userCollage)
      resolve()
    })
  },
  rightSidebarShown ({ commit }, isRightSidebarShown) {
    return new Promise(function (resolve) {
      commit(RIGHT_SIDEBAR_SHOWN, isRightSidebarShown)
      resolve()
    })
  },
  relatedModelsSet ({ commit }, payload) {
    return new Promise(function (resolve) {
      commit(RELATED_MODELS_SET, payload)
      resolve()
    })
  },
  relatedObjectsSet ({ commit }, payload) {
    return new Promise(function (resolve) {
      commit(RELATED_OBJECTS_SET, payload)
      resolve()
    })
  },
  relatedBackgroundsSet ({ commit }, payload) {
    return new Promise(function (resolve) {
      commit(RELATED_BACKGROUNDS_SET, payload)
      resolve()
    })
  },
  setUserCollages ({ commit }, payload) {
    return new Promise(function (resolve) {
      commit(USER_COLLAGES_SET, payload)
      resolve()
    })
  },
  errorsSet ({ commit }, errors) {
    return new Promise(function (resolve) {
      commit(ERRORS_SET, errors)
      resolve()
    })
  },
  clearStore ({ commit }) {
    return new Promise(function (resolve) {
      commit(CLEAR_STORE)
      resolve()
    })
  },
  setSearchModels ({ commit }, payload) {
    return new Promise(function (resolve) {
      commit(SEARCH_MODELS_SET, payload)
      resolve()
    })
  },
  setSearchObjects ({ commit }, payload) {
    return new Promise(function (resolve) {
      commit(SEARCH_OBJECTS_SET, payload)
      resolve()
    })
  },
  setSearchBackgrounds ({ commit }, payload) {
    return new Promise(function (resolve) {
      commit(SEARCH_BACKGROUNDS_SET, payload)
      resolve()
    })
  },
  setLicenseVerified ({ commit }, isLicenseVerified) {
    return new Promise(function (resolve) {
      commit(LICENSE_VERIFIED, isLicenseVerified)
      resolve()
    })
  },
  updateCollage ({ commit }, collageData) {
    return new Promise(function (resolve) {
      commit(USER_COLLAGE_UPDATED, collageData)
      resolve()
    })
  },
  setStageSizesShown ({ commit }, stageSizesShown) {
    return new Promise(function (resolve) {
      commit(STAGE_SIZES_SHOWN, stageSizesShown)
      resolve()
    })
  },
  setCurrentAction ({ commit }, currentAction) {
    return new Promise(function (resolve) {
      commit(CURRENT_ACTION, currentAction)
      resolve()
    })
  },
  setIsMobile ({ commit }, isMobile) {
    return new Promise(function (resolve) {
      commit(IS_MOBILE, isMobile)
      resolve()
    })
  },
  setIsPortrait ({ commit }, isPortrait) {
    return new Promise(function (resolve) {
      commit(IS_PORTRAIT, isPortrait)
      resolve()
    })
  },
  setRelatedToStageItems ({ commit }, relatedToStageItems) {
    return new Promise(function (resolve) {
      commit(RELATED_TO_STAGE_ITEMS_SET, relatedToStageItems)
      resolve()
    })
  },
  setRelatedToStageItemsLoading ({ commit }) {
    return new Promise(function (resolve) {
      commit(RELATED_TO_STAGE_ITEMS_LOADING)
      resolve()
    })
  },
  setRelatedToStageItemsLoaded ({ commit }) {
    return new Promise(function (resolve) {
      commit(RELATED_TO_STAGE_ITEMS_LOADED)
      resolve()
    })
  },
  setRelatedItemsListShown ({ commit }, relatedItemsListShown) {
    return new Promise(function (resolve) {
      commit(RELATED_ITEMS_LIST_SHOWN, relatedItemsListShown)
    })
  },
  addCurrentUpload ({ commit }, upload) {
    return new Promise(function (resolve) {
      commit(ADD_UPLOAD, upload)
      resolve()
    })
  },
  updateCurrentUpload ({ commit }, { uploadingId, payload }) {
    return new Promise(function (resolve) {
      commit(UPDATE_UPLOAD, { uploadingId, payload })
      resolve()
    })
  },
  deleteCurrentUpload ({ commit }, uploadId) {
    return new Promise(function (resolve) {
      commit(DELETE_UPLOAD, uploadId)
      resolve()
    })
  },
  setFaceSelected ({ commit }, faceSelected) {
    return new Promise(function (resolve) {
      commit(FACE_SELECTED, faceSelected)
      resolve()
    })
  },
  setAutosaveStatus ({ commit }, autosaveStatus) {
    return new Promise(function (resolve) {
      commit(AUTOSAVE_STATUS_SET, autosaveStatus)
      resolve()
    })
  }
}

const store = () => ({
  strict: process.env.NODE_ENV !== 'production',
  getters,
  state,
  mutations,
  actions
})

export default store
