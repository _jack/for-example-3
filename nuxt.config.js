'use strict'

const defaultConfig = require('icons8-common/src/nuxt.defaults')
const config = Object.assign({}, defaultConfig)

config.css = [{ src: './assets/css/public.scss', lang: 'scss' }]

config.plugins = [
  '~plugins/init',
  '~plugins/components',
  { src: '~/plugins/vueMasonry', ssr: false },
  { src: '~/plugins/vueWaypoint', ssr: false },
  { src: '~/plugins/vueMouseParallax' }
]

config.modules = [["icons8-common/src/module.js", { "hreflang": false }]]

const favicon = 'https://photos.icons8.com/vue-static/photos/favicon.png'
config.favicon = [
  { size: 16, src: favicon },
  { size: 32, src: favicon },
  { size: 96, src: favicon },
  { size: 196, src: favicon }
]

module.exports = config
