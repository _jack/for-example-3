'use strict'

function importAll (plugin, r) {
  r.keys().forEach(key => {
    const id = key.split('./').join('').split('.svg').join('')
    const href = r(key).default.id
    plugin[id] = `<svg width="100%" height="100%"><use xlink:href="#${href}"></use></svg>`
  })
}

export default (context, inject) => {
  const $icons = {}
  importAll($icons, require.context(`!!svg-sprite-loader!D:\\__Work\\icons8\\icons8-photos/assets/svg`, true, /\.*$/))
  inject('icons', $icons)
}
