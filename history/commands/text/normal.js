'use strict'

import storeService from '../../../services/store'
import stageService from '../../../services/stage'

export default class NormalCommand {
  constructor (item, currentFontStyle) {
    this.item = item
    this.currentFontStyle = currentFontStyle
    this.previousFontStyle = this.item.fontStyle()
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontStyle = this.previousFontStyle
        textarea.style.fontWeight = this.previousFontStyle
        textarea.focus()
      }
      this.item.fontStyle(this.previousFontStyle)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const fontStyle = 'normal'
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontStyle = fontStyle
        textarea.style.fontWeight = fontStyle
        textarea.focus()
      }
      this.item.fontStyle(fontStyle)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }
}
