'use strict'

import storeService from '../../services/store'
import stageService from '../../services/stage'

export default class ResizeStageCommand {
  constructor (stageWidth, stageHeight) {
    this.previousStageWidth = storeService.get('vuex').state.editor.stage.realWidth
    this.previousStageHeight = storeService.get('vuex').state.editor.stage.realHeight
    this.nextStageWidth = stageWidth
    this.nextStageHeight = stageHeight
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      stageService.resize(this.previousStageWidth, this.previousStageHeight)
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      stageService.resize(this.nextStageWidth, this.nextStageHeight)
      resolve()
    })
  }
}
