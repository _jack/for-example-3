import Konva from 'konva'
import config from '../../config'
import backgroundService from './background'
import storeService from '../store'
import stageService from '../stage'
import itemsService from './common'
import faceRectangleService from '../items/faceRectangle'
import photosApi from '../../api/photos'

const SORT_NEW = 'SORT_NEW'
const SORT_RISING = 'SORT_RISING'
const SORT_TOP = 'SORT_TOP'
const SORT_FACE_SWAP = 'SORT_FACE_SWAP'
const SORTINGS = [
  {
    id: SORT_NEW,
    title: 'New'
  },
  {
    id: SORT_RISING,
    title: 'Rising'
  },
  {
    id: SORT_TOP,
    title: 'Top'
  },
  {
    id: SORT_FACE_SWAP,
    title: 'Swap'
  }
]

async function sort (sortingType) {
  const store = storeService.get('vuex')
  const searchQuery = store.state.editor.searchQuery

  // If it is a new sorting - sort them all
  if (sortingType !== store.state.editor[searchQuery ? `searchModels` : `models`].sorting) {
    const params = Object.assign(getSortingParams(sortingType), { query: searchQuery })
    await photosApi.models(params)
      .then(response => {
        store.dispatch(
          searchQuery ? `setSearchModels` : `setModels`,
          {
            items: handlePhotos(response['models']),
            total: response.total,
            page: 1,
            sorting: sortingType
          })
      })
  }
}

function getSortingParams (sortingType) {
  let params = {}
  switch (sortingType) {
    case SORT_NEW:
      params.sort_by = 'created'
      params.direction = 'desc'
      break
    case SORT_RISING:
      params.sort_by = 'rising'
      params.direction = 'desc'
      break
    case SORT_TOP:
      params.sort_by = 'top'
      params.direction = 'desc'
      break
    case SORT_FACE_SWAP:
      params.filter = 'with_face_swappable'
      params.direction = 'desc'
    break
  }

  return params
}

function calculatePositionAndSize (model) {
  const isLandscape = model.width / model.height > 1.2
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  let ratio, width, height, x, y

  // Calculate scale
  if (isLandscape) {
    // First calculate scale by width
    ratio = actualSizesAndPosition.width / model.width

    // Check if scaled height is less than stage's height percent
    if (model.height * ratio > actualSizesAndPosition.height * config.modelMaxHeightPercent / 100) {
      // Scale by width is too big, take scale by height's percent
      ratio = actualSizesAndPosition.height * config.modelMaxHeightPercent / 100 / model.height
    }
  } else {
    // Calculate scale by model max height percent
    ratio = actualSizesAndPosition.height * config.modelMaxHeightPercent / 100 / model.height

    // Check if scaled width is less than stage's width
    if (model.width * ratio > actualSizesAndPosition.width) {
      // Scale by width is too big, take scale by height's percent
      ratio = actualSizesAndPosition.width / model.width
    }
  }

  width = model.width * ratio
  height = model.height * ratio

  // Calculate coordinates
  const coordinates = backgroundService.getImageCoordinates(model, width, height)
  x = coordinates ? coordinates.x : actualSizesAndPosition.x + width / 2
  y = coordinates && model.point ? coordinates.y : actualSizesAndPosition.y + actualSizesAndPosition.height - height + height / 2

  // If the bottom of the model is above the stage's bottom - enlarge the model to the stage's bottom
  if (model.point && y + height / 2 < actualSizesAndPosition.y + actualSizesAndPosition.height) {
    const pointYRatio = model.point.y / model.assetHeight
    const difference = actualSizesAndPosition.y + actualSizesAndPosition.height - (y + height / 2)
    const heightRatio = (height + difference + difference * (pointYRatio / (1 - pointYRatio))) / height
    width *= heightRatio
    height *= heightRatio
    const coordinates = backgroundService.getImageCoordinates(model, width, height)
    x = coordinates.x
    y = coordinates.y
  }

  // If model is outside the stage - move it to the stage
  if (x + width / 2 > actualSizesAndPosition.x + actualSizesAndPosition.width) {
    x = actualSizesAndPosition.x + actualSizesAndPosition.width - width / 2
  }
  if (x - width / 2 < actualSizesAndPosition.x) {
    x = actualSizesAndPosition.x + width / 2
  }

  return { width, height, x, y }
}

function handlePhotos (assetObjects, add = false, store = 'default') {
  let models = []
  assetObjects.forEach(assetObject => {
    const model = itemsService.handleAssetObject(itemsService.TYPE_MODEL, assetObject)
    models.push(model)
  })

  if (add) {
    let currentModels
    switch (store) {
      case 'default':
        currentModels = storeService.get('vuex').state.editor.models.items
        break
      case 'search':
        currentModels = storeService.get('vuex').state.editor.searchModels.items
        break
      case 'related':
        currentModels = storeService.get('vuex').state.editor.related.models.items
        break
      case 'alternative':
        currentModels = storeService.get('vuex').state.editor.related.toItemItems.items
        break
    }
    models = [...currentModels, ...models]
  }

  return models
}

function checkIfFaceReplacable (objectData) {
  return objectData.type === itemsService.TYPE_MODEL && objectData.face && objectData.face.replaceable
}

/*
 * Check for swapped faces
 */
function handleSwappedFaces (model) {
  return new Promise(resolve => {
    const objectData = model.getAttrs().objectData
    if (checkIfFaceReplacable(objectData)) {
      photosApi.getSwappedFacesByModel(objectData.id)
        .then(faces => {
          if (faces.total) {
            itemsService.updateObjectData(
              model,
              Object.assign(
                {},
                model.getAttrs().objectData, {
                  swappedFacesData: {
                    faces: faces.swappedFaces,
                    total: faces.total,
                    page: 1
                  }
                })
            )
            faceRectangleService.addFaceRectangle(model)
          }
          resolve()
        })
    } else {
      resolve()
    }
  })
}

function swapFace (model, face) {
  const modelObjectData = model.getAttrs().objectData
  const faceSizeAndCoordinates = model.getAttrs().objectData.faceRect
  const stage = storeService.get('stage')
  const scale = model.width() / model.getAttrs().objectData.width

  // Delete old swapped faces if exist
  model.find('.itemSwappedFace').destroy()

  // Create masked face image
  return Promise.all([
    itemsService.getImageData(face.url),
    itemsService.getImageData(
      modelObjectData.maskAsset.url,
      faceSizeAndCoordinates.x,
      faceSizeAndCoordinates.y,
      faceSizeAndCoordinates.width,
      faceSizeAndCoordinates.height
    )
  ])
    .then(images => {
      return itemsService.mergeImages(images[0], images[1])
        .then(image => {
          const faceImage = new Konva.Image({
            name: 'itemSwappedFace',
            width: faceSizeAndCoordinates.width * scale,
            height: faceSizeAndCoordinates.height * scale,
            x: faceSizeAndCoordinates.x * scale,
            y: faceSizeAndCoordinates.y * scale,
            image
          })
          model.add(faceImage)
          itemsService.updateObjectData(model, Object.assign({}, model.getAttrs().objectData, { currentSwappedFace: face }))

          // Move face border rectange to the top of group
          const faceRectangleGroup = model.findOne('.faceRectangleGroup')
          if (faceRectangleGroup) {
            faceRectangleGroup.moveToTop()
          }
          stage.findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
        })
    })
}

function recalculateSwappedFacePositionAndSize (model) {
  const swappedFace = model.find('.itemSwappedFace')
  if (swappedFace) {
    const faceSizeAndCoordinates = model.getAttrs().objectData.faceRect
    const scale = model.width() / model.getAttrs().objectData.width
    swappedFace.setAttrs({
      width: faceSizeAndCoordinates.width * scale,
      height: faceSizeAndCoordinates.height * scale,
      x: faceSizeAndCoordinates.x * scale,
      y: faceSizeAndCoordinates.y * scale
    })
  }
}

export default {
  recalculateSwappedFacePositionAndSize,
  calculatePositionAndSize,
  handlePhotos,
  swapFace,
  handleSwappedFaces,
  checkIfFaceReplacable,
  getSortingParams,
  sort,
  SORT_NEW,
  SORT_RISING,
  SORT_TOP,
  SORT_FACE_SWAP,
  SORTINGS
}
