'use strict'

import storeService from '../../services/store'
import stageService from '../../services/stage'

export default class FlipImageCommand {
  constructor (item, axis) {
    this.item = item
    this.axis = axis
    this.scaleXValue = item.attrs.scaleX
    this.scaleYValue = item.attrs.scaleY
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      switch (true) {
        case this.axis === 'X':
          this.item.scaleX(this.scaleXValue)
          break
        case this.axis === 'Y':
          this.item.scaleY(this.scaleYValue)
          break
      }
      storeService
        .get('stage')
        .findOne(`#${stageService.ITEMS_LAYER_ID}`)
        .draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      switch (true) {
        case this.axis === 'X':
          this.item.scaleX(this.scaleXValue * -1)
          break
        case this.axis === 'Y':
          this.item.scaleY(this.scaleYValue * -1)
          break
      }
      storeService
        .get('stage')
        .findOne(`#${stageService.ITEMS_LAYER_ID}`)
        .draw()

      resolve()
    })
  }
}
