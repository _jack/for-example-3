import Konva from 'konva'
import storeService from '../store'
import stageService from '../stage'
import itemsService from './common'
import config from '../../config'
import photosApi from '../../api/photos'

const SORT_NEW = 'SORT_NEW'
const SORT_RISING = 'SORT_RISING'
const SORT_TOP = 'SORT_TOP'
const SORTINGS = [
  {
    id: SORT_NEW,
    title: 'New'
  },
  {
    id: SORT_RISING,
    title: 'Rising'
  },
  {
    id: SORT_TOP,
    title: 'Top'
  }
]

const DEFAULT_BACKGROUND_LAYER_ID = 'default_background'

async function sort (sortingType) {
  const store = storeService.get('vuex')

  // If it is a new sorting - sort them all
  if (sortingType !== store.state.editor.backgrounds.sorting) {
    await photosApi.backgrounds(getSortingParams(sortingType))
      .then(response => {
        store.dispatch(
          `setBackgrounds`,
          {
            items: handlePhotos(response['backgrounds']),
            total: response.total,
            page: 1,
            sorting: sortingType
          })
      })
  }
}

function getSortingParams (sortingType) {
  let params = {}
  switch (sortingType) {
    case SORT_NEW:
      params.sort_by = 'created'
      params.direction = 'desc'
      break
    case SORT_RISING:
      params.sort_by = 'rising'
      params.direction = 'desc'
      break
    case SORT_TOP:
      params.sort_by = 'top'
      params.direction = 'desc'
      break
  }

  return params
}

function calculatePositionAndSize (background) {
  // First calculate ratio by width
  let ratio = storeService.get('vuex').state.editor.stage.width / background.width

  // Check if scaled height is less than stage's height
  if (background.height * ratio < storeService.get('vuex').state.editor.stage.height) {
    // Ratio by width is too small, take ratio by height
    ratio = storeService.get('vuex').state.editor.stage.height / background.height
  }

  const width = background.width * ratio
  const height = background.height * ratio
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  let x = actualSizesAndPosition.x + actualSizesAndPosition.width / 2
  let y

  // Align horizontal background to the top and to the center if vertical
  if (width / height > 1) {
    y = actualSizesAndPosition.y + height / 2
  } else {
    // If background and any model have points - set background Y point to the model Y point
    if (background.modelPoints && background.modelPoints.length) {
      let backgroundCorrectionY = height / 2 - height * (background.modelPoints[0].y / background.height)
      storeService.get('items').forEach(item => {
        const itemData = item.getAttrs().objectData
        if (!y && itemData && itemData.type === itemsService.TYPE_MODEL && itemData.point) {
          let modelPoint = itemData.point
          y = item.height() * (modelPoint.y / itemData.assetHeight) + (item.y() - item.height() / 2) + backgroundCorrectionY
        }
      })
    }

    if (!y) {
      y = actualSizesAndPosition.y + actualSizesAndPosition.height / 2
    }
  }

  // Move if background points become hidden at this position -
  // move background to show them
  if (background.modelPoints || background.objectPoints) {
    const backgroundPoints = [...background.modelPoints, ...background.objectPoints]
    backgroundPoints.forEach(backgroundPoint => {
      const backgroundPointX = width * (backgroundPoint.x / background.width)
      const backgroundPointY = height * (backgroundPoint.y / background.height)
      if (backgroundPointX > width / 2 + actualSizesAndPosition.width / 2) {
        x -= backgroundPointX - (width / 2 + actualSizesAndPosition.width / 2)
      } else if (backgroundPointX < width / 2 - actualSizesAndPosition.width / 2) {
        x += width / 2 - actualSizesAndPosition.width / 2 - backgroundPointX
      } else if (backgroundPointY > actualSizesAndPosition.height) {
        y = y - (backgroundPointY - actualSizesAndPosition.height)
      }
    })
  }
  return { width, height, x, y }
}

function handle (object, params = {}, restore = false, silent = false, skilpOutsideHandling = true) {
  if (storeService.get('background')) {
    storeService.destroyBackground()
  }

  return itemsService.handleImage(object, params, false, restore, silent, skilpOutsideHandling)
}

/*
 * Create default background
 */
function initDefaultBackground (fillColor = config.defaultBackgroundColor) {
  return new Promise(resolve => {
    const stage = storeService.get('stage')
    const defaultBackgroundParams = {
      type: itemsService.TYPE_DEFAULT_BACKGROUND,
      width: stage.width(),
      height: stage.height()
    }
    const layer = stageService.createLayer({
      id: DEFAULT_BACKGROUND_LAYER_ID,
      name: DEFAULT_BACKGROUND_LAYER_ID
    })
    stage.add(layer)

    // Default background
    const defaultBackground = new Konva.Rect(defaultBackgroundParams)
    storeService.set('defaultBackground', defaultBackground)
    defaultBackground.fill(fillColor)
    stageService.addToLayer(layer, defaultBackground)

    // Events
    const clickHandler = () => {
      itemsService.deactivateItem()
      storeService.get('vuex').commit('setDefaultBackgroundActive', true)
    }
    layer
      .on('tap', (event) => {
        clickHandler(event)
      })
      .on('mousedown', (event) => {
        clickHandler(event)
      })

    layer.draw()
    resolve()
  })
}

function getImageCoordinates (object, objectWidth, objectHeight) {
  let backgroundPoints = getBackgroundPoints(object)
  return backgroundPoints ? calculateImagePosition(backgroundPoints, object, objectWidth, objectHeight) : undefined
}

function getBackgroundPoints (object) {
  let backgroundPoints

  if (storeService.get('background')) {
    const backgroundObjectData = storeService.get('background').getAttrs().objectData
    switch (object.type) {
      case itemsService.TYPE_MODEL:
        if (backgroundObjectData.modelPoints && backgroundObjectData.modelPoints.length) {
          backgroundPoints = backgroundObjectData.modelPoints
        }
        break
      case itemsService.TYPE_OBJECT:
        if (backgroundObjectData.objectPoints && backgroundObjectData.objectPoints.length) {
          backgroundPoints = backgroundObjectData.objectPoints
        }
        break
    }
  }

  return backgroundPoints
}

function calculateImagePosition (backgroundPoints, object, itemWidth, itemHeight) {
  const background = storeService.get('background')

  // Calculate background point coordinates
  const backgroundObjectData = background.getAttrs().objectData
  const backgroundPoint = backgroundPoints[0]
  let backgroundPointX = background.width() * (backgroundPoint.x / backgroundObjectData.width) + (background.x() - background.width() / 2)
  let backgroundPointY = background.height() * (backgroundPoint.y / backgroundObjectData.height) + (background.y() - background.height() / 2)

  // Calculate item point coordinates
  let itemCorrectionX = 0
  let itemCorrectionY = 0
  const itemPoint = object.point
  if (itemPoint) {
    itemCorrectionX = itemWidth / 2 - itemWidth * (itemPoint.x / object.assetWidth)
    itemCorrectionY = itemHeight / 2 - itemHeight * (itemPoint.y / object.assetHeight)
  }

  return {
    x: backgroundPointX + itemCorrectionX,
    y: backgroundPointY + itemCorrectionY
  }
}

function handlePhotos (assetObjects, add = false, store = 'default') {
  let backgrounds = []
  assetObjects.forEach(assetObject => {
    const background = itemsService.handleAssetObject(itemsService.TYPE_BACKGROUND, assetObject)
    backgrounds.push(background)
  })

  if (add) {
    let currentBackgrounds
    switch (store) {
      case 'default':
        currentBackgrounds = storeService.get('vuex').state.editor.backgrounds.items
        break
      case 'search':
        currentBackgrounds = storeService.get('vuex').state.editor.searchBackgrounds.items
        break
      case 'related':
        currentBackgrounds = storeService.get('vuex').state.editor.related.backgrounds.items
        break
      case 'alternative':
        currentBackgrounds = storeService.get('vuex').state.editor.related.toItemItems.items
        break
    }
    backgrounds = [...currentBackgrounds, ...backgrounds]
  }

  return backgrounds
}

export default {
  getBackgroundPoints,
  handle,
  handlePhotos,
  initDefaultBackground,
  getImageCoordinates,
  calculatePositionAndSize,
  sort,
  getSortingParams,
  DEFAULT_BACKGROUND_LAYER_ID,
  SORT_NEW,
  SORT_RISING,
  SORT_TOP,
  SORTINGS

}
