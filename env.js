'use strict'

const port = process.env.PORT || 3037

const env = {
  default: {
    port,
    project: 'photos',
    baseURL: 'https://demo-photos.icons8.com/api/v1',
    apiUrl: 'https://demo-photos.icons8.com/api/v1',
    host: 'https://demo-photos.icons8.com'
  },
  dev: {
    // staticUrl: 'https://demo-photos.icons8.com',
    apiUrl: 'https://demo-photos.icons8.com/api/v1',
    host: 'https://demo-photos.icons8.com'
  },
  production: {
    // staticUrl: 'https://photo-creator-cdn.icons8.com',
    apiUrl: 'https://photos.icons8.com/api/v1',
    host: 'https://photos.icons8.com'
  }
}

module.exports = env
