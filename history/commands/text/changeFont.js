'use strict'

import storeService from '../../../services/store'
import stageService from '../../../services/stage'
import textService from '../../../services/items/text'

export default class ChangeFontCommand {
  constructor (item, fontFamily) {
    this.item = item
    this.previousFontFamily = item.fontFamily()
    this.fontFamily = fontFamily
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontFamily = this.previousFontFamily
        textarea.focus()
      }
      this.item.fontFamily(this.previousFontFamily)
      textService.resizeTextNode(this.item)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const fontFamily = this.fontFamily
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontFamily = fontFamily
        textarea.focus()
      }
      this.item.fontFamily(this.fontFamily)
      textService.resizeTextNode(this.item)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }
}
