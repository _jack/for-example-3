'use strict'

import storeService from '../../../services/store'
import stageService from '../../../services/stage'

export default class BoldCommand {
  constructor (item) {
    this.item = item
    this.previousFontStyle = this.item.fontStyle()
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontWeight = this.previousFontStyle
        textarea.style.fontStyle = 'normal'
        textarea.focus()
      }
      this.item.fontStyle(this.previousFontStyle)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const fontWeight = 'bold'
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontWeight = fontWeight
        textarea.style.fontStyle = 'normal'
        textarea.focus()
      }
      this.item.fontStyle(fontWeight)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }
}
