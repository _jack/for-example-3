/** plugin который загружает переводы из редиса (если есть) или из файлов самой репы.
 *  В редисе всегда актуальные переводы, в репе они могут быть устаревшие.
 *  в редис они попадают из gitlab
 */

const debug = require('debug')('common:i18n')
const LRU = require('lru-cache')

// кеш переводов, чтобы не загружать их заного при каждом запросе
const cache = new LRU({
  max: 10000,
  maxAge: 1000 * 60 * 60 * 2 // 24 hours
})

function getKey (project, locale) {
  return `translations[${project}][${locale}]`
}

export default ({ store, app }, inject) => {
  debug('register i18n plugin')

  // Метод который осуществляется перевод
  const $t = function (key, props, defaultString) {
    key = '' + key
    let keys = key.split('.')
    let value
    if (store.state.i18n.locale) {
      value = store.state.i18n.translation
      if (this.$store) {
        value = this.$store.state.i18n.translation
      }
    }
    keys.some((k) => {
      if (value) {
        value = value[k]
      } else {
        return true
      }
    })
    if (typeof props === 'object' && typeof value === 'string') {
      Object.keys(props).forEach(prop => {
        value = value.split(`{{${prop}}}`).join(props[prop])
      })
    }
    if (typeof props === 'string' && value === undefined) {
      value = props
    }
    if (typeof defaultString === 'string' && value === undefined) {
      value = defaultString
      if (process.browser) {
        console.warn(`Translation key ${key} not found. Check translation files`)
      }
    }
    return value !== undefined ? value : key.toLocaleLowerCase()
  }

  // указаываем проект для которого подгружаются переводы
  // по умолчанию для всех проектов подгружаются также переводы для этого проекта common
  $t.project = process.env.project

  // метод с помощью которого загружаем переводы
  $t.loadLocale = function (locale) {
    const project = process.env.project
    return new Promise(function (resolve) {
      // пробуем извлечь из кеша
      $t.loadFromCache()
        .then(translations => {
          store.commit('SET_TRANSLATION', translations)
          resolve()
        })
        // иначе загружаем наппрямую из проекта
        .catch(() => {
          debug(`locale path D:\\__Work\\icons8\\icons8-photos/translations/${project}/${locale}.json`)
          let translations
          if (project === 'common') {
            translations = require(`D:\\__Work\\icons8\\icons8-photos/translations/common/${locale}.json`)
          } else {
            translations = Object.assign(
              require(`D:\\__Work\\icons8\\icons8-photos/translations/common/${locale}.json`),
              require(`D:\\__Work\\icons8\\icons8-photos/translations/${project}/${locale}.json`)
            )
          }
          cache.set(getKey($t.project, locale), translations)
          store.commit('SET_TRANSLATION', translations)
          resolve()
        })
    })
  }

  $t.loadFromCache = function (locale) {
    return new Promise(function (resolve, reject) {
      // пробуем извлечь из in-memory кеша
      const translations = cache.get(getKey($t.project, locale))
      if (translations) {
        resolve(translations)
        return
      }
      // пробуем извлечь из redis
      app.$apiCache.get(getKey($t.project, locale))
        .then(translations => {
          cache.set(getKey($t.project, locale), translations)
          if (translations) {
            resolve(translations)
          } else {
            reject()
          }
        })
        .catch(reject)
    })
  }

  inject('t', $t)
}
