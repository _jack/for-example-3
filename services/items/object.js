import storeService from '../store'
import stageService from '../stage'
import itemsService from './common'
import backgroundService from './background'
import photosApi from '../../api/photos'

const SORT_NEW = 'SORT_NEW'
const SORT_RISING = 'SORT_RISING'
const SORT_TOP = 'SORT_TOP'
const SORTINGS = [
  {
    id: SORT_NEW,
    title: 'New'
  },
  {
    id: SORT_RISING,
    title: 'Rising'
  },
  {
    id: SORT_TOP,
    title: 'Top'
  }
]

async function sort (sortingType) {
  const store = storeService.get('vuex')

  // If it is a new sorting - sort them all
  if (sortingType !== store.state.editor.sorting) {
    await photosApi.objects(getSortingParams(sortingType))
      .then(response => {
        store.dispatch(
          `setObjects`,
          {
            items: handlePhotos(response['objects']),
            total: response.total,
            page: 1,
            sorting: sortingType
          })
      })
  }
}

function getSortingParams (sortingType) {
  let params = {}
  switch (sortingType) {
    case SORT_NEW:
      params.sort_by = 'created'
      params.direction = 'desc'
      break
    case SORT_RISING:
      params.sort_by = 'rising'
      params.direction = 'desc'
      break
    case SORT_TOP:
      params.sort_by = 'top'
      params.direction = 'desc'
      break
  }

  return params
}

function calculatePositionAndSize (object) {
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  const scale = fitObject(object, 0.5)
  let width = object.width * scale
  let height = object.height * scale
  const coordinates = backgroundService.getImageCoordinates(object, width, height)
  let x = coordinates ? coordinates.x : actualSizesAndPosition.x + actualSizesAndPosition.width / 2
  let y = coordinates ? coordinates.y : actualSizesAndPosition.y + actualSizesAndPosition.height - height / 2

  // If object is outside the stage - move it to the stage
  if (x + width / 2 > actualSizesAndPosition.x + actualSizesAndPosition.width) {
    x = actualSizesAndPosition.x + actualSizesAndPosition.width - width / 2
  }
  if (x - width / 2 < actualSizesAndPosition.x) {
    x = actualSizesAndPosition.x + width / 2
  }
  if (y + width / 2 > actualSizesAndPosition.y + actualSizesAndPosition.height) {
    y = actualSizesAndPosition.y + actualSizesAndPosition.height - height / 2
  }
  if (y - height / 2 < actualSizesAndPosition.y) {
    y = actualSizesAndPosition.y + height / 2
  }

  return { width, height, x, y }
}

function fitObject (object, ratio = 1) {
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  const objectWidth = object.width
  const objectHeight = object.height

  let ratioWidth = actualSizesAndPosition.width / objectWidth
  let ratioHeight = actualSizesAndPosition.height / objectHeight

  const stageRatio = actualSizesAndPosition.width / actualSizesAndPosition.height
  const objectRation = objectWidth / objectHeight
  if (stageRatio >= objectRation) {
    return ratioHeight * ratio
  } else {
    return ratioWidth * ratio
  }
}

function handlePhotos (assetObjects, add = false, store = 'default') {
  let objects = []

  assetObjects.forEach(assetObject => {
    const object = itemsService.handleAssetObject(itemsService.TYPE_OBJECT, assetObject)
    objects.push(object)
  })

  if (add) {
    let currentObjects
    switch (store) {
      case 'default':
        currentObjects = storeService.get('vuex').state.editor.objects.items
        break
      case 'search':
        currentObjects = storeService.get('vuex').state.editor.searchObjects.items
        break
      case 'related':
        currentObjects = storeService.get('vuex').state.editor.related.objects.items
        break
      case 'alternative':
        currentObjects = storeService.get('vuex').state.editor.related.toItemItems.items
        break
    }
    objects = [...currentObjects, ...objects]
  }

  return objects
}

export default {
  calculatePositionAndSize,
  handlePhotos,
  sort,
  getSortingParams,
  SORT_NEW,
  SORT_RISING,
  SORT_TOP,
  SORTINGS

}
