'use strict'

import storeService from '../../services/store'
import stageService from '../../services/stage'
import itemsService from '../../services/items/common'

export default class ReplaceItemCommand {
  constructor (itemToReplace, replacingItemData, silent = false) {
    this.itemToReplace = itemToReplace
    this.replacingItemData = replacingItemData
    this.replacingItem = undefined
    this.silent = silent
    this.replacingItemParams = itemsService.calculateRelated(this.itemToReplace, this.replacingItemData)
  }

  execute () {
    this.itemToReplace.remove()
    const itemZIndex = this.itemToReplace.getZIndex()
    return itemsService.handleImage(this.replacingItemData, this.replacingItemParams, true, true, this.silent)
      .then(item => {
        this.replacingItem = item
        this.replacingItem.setZIndex(itemZIndex)
        storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      })
  }

  undo () {
    return new Promise(resolve => {
      this.replacingItem.remove()
      const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
      const itemZIndex = this.itemToReplace.getZIndex()
      stageService.addToLayer(itemsLayer, this.itemToReplace)
      this.itemToReplace.setZIndex(itemZIndex)
      itemsLayer.draw()

      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      this.itemToReplace.remove()
      const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
      const itemZIndex = this.replacingItem.getZIndex()
      stageService.addToLayer(itemsLayer, this.replacingItem)
      this.replacingItem.setZIndex(itemZIndex)
      itemsLayer.draw()

      resolve()
    })
  }
}
