'use strict'

import Vuex from 'vuex'
import editor from './editor'
import landing from './landing'

const store = () => new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    editor: editor(),
    landing: landing()
  }
})

export default store
