import Konva from 'konva'
import storeService from './store'
import dataService from './data'
import backgroundService from './items/background'
import itemsService from './items/common'
import fileSaver from 'file-saver'
import config from '../config'
import keyboardEvents from './keyboardEvents'
import DeleteItemCommand from '../history/commands/deleteItem'
import ResizeStageCommand from '../history/commands/resizeStage'
import faceRectangleService from './items/faceRectangle'

const CLIPPING_GROUP_NAME = 'clippingGroup'
const DRAGGING_IMAGE_LAYER_ID = 'draggingImage'
const ITEMS_LAYER_ID = 'items_layer'
const TYPE_TEXT_COLOR_PICKER = 'textColorPicker'
let EVENTS_SET = false

function calculateRotationAngle (currentAngle) {
  if (currentAngle < 0) {
    currentAngle = 360 + currentAngle % 360
  }
  return Math.abs(currentAngle % 360)
}

function handleEvents () {
  // Adding keypress events
  document.onkeydown = function (event) {
    event = event || window.event
    const stage = storeService.get('stage')

    switch (true) {
      case keyboardEvents.is(event, keyboardEvents.ESCAPE):
        if (storeService.get('activeItem')) {
          itemsService.deactivateItem()
        } else {
          stage.find('Transformer').destroy()
          stage.findOne(`#${ITEMS_LAYER_ID}`).draw()
        }
        break
      case keyboardEvents.is(event, keyboardEvents.DELETE):
      case keyboardEvents.is(event, keyboardEvents.BACKSPACE):
        if (storeService.get('activeItem')) {
          const activeElementTagName = document.activeElement.tagName
          if (!['INPUT', 'TEXTAREA'].includes(activeElementTagName)) {
            storeService.get('commandManager').execute(new DeleteItemCommand(storeService.get('activeItem')))
          }
        }
        break
      case keyboardEvents.is(event, keyboardEvents.ARROW_DOWN):
        handleArrowsEvents(keyboardEvents.ARROW_DOWN)
        break
      case keyboardEvents.is(event, keyboardEvents.ARROW_UP):
        handleArrowsEvents(keyboardEvents.ARROW_UP)
        break
      case keyboardEvents.is(event, keyboardEvents.ARROW_LEFT):
        handleArrowsEvents(keyboardEvents.ARROW_LEFT)
        break
      case keyboardEvents.is(event, keyboardEvents.ARROW_RIGHT):
        handleArrowsEvents(keyboardEvents.ARROW_RIGHT)
        break
      case keyboardEvents.is(event, keyboardEvents.Z, (event.ctrlKey || event.metaKey) && event.shiftKey):
        if (event.target.tagName.toLowerCase() !== 'input') {
          storeService.get('commandManager').redo()
          event.preventDefault()
        }
        break
      case keyboardEvents.is(event, keyboardEvents.Z, event.ctrlKey || event.metaKey):
        if (event.target.tagName.toLowerCase() !== 'input') {
          storeService.get('commandManager').undo()
          event.preventDefault()
        }
        break
    }
  }
}

function handleArrowsEvents (keyboardEvent) {
  const stage = storeService.get('stage')
  const activeElementTagName = document.activeElement.tagName

  if (stage.find('Transformer').length && !['INPUT', 'TEXTAREA'].includes(activeElementTagName)) {
    let transformerNode = stage.find('Transformer')[0].getNode()

    switch (true) {
      case keyboardEvent === keyboardEvents.ARROW_DOWN:
        transformerNode.y(transformerNode.y() + config.itemsMoveStep)
        dataService.handleCollageSaving(true)
        break
      case keyboardEvent === keyboardEvents.ARROW_UP:
        transformerNode.y(transformerNode.y() - config.itemsMoveStep)
        dataService.handleCollageSaving(true)
        break
      case keyboardEvent === keyboardEvents.ARROW_LEFT:
        transformerNode.x(transformerNode.x() - config.itemsMoveStep)
        dataService.handleCollageSaving(true)
        break
      case keyboardEvent === keyboardEvents.ARROW_RIGHT:
        transformerNode.x(transformerNode.x() + config.itemsMoveStep)
        dataService.handleCollageSaving(true)
        break
    }

    itemsService.handleDraggingCoordinates(transformerNode, true)
    itemsService.refreshTools()
    stage.findOne(`#${ITEMS_LAYER_ID}`).draw()
  }
}

function handleOutsideClick (deactivateDefaultBackground = true) {
  itemsService.deactivateItem()

  if (deactivateDefaultBackground) {
    storeService.get('vuex').commit('setDefaultBackgroundActive', false)
  }

  // Hide all face rectangles
  if (!isInsideStageCoordinates()) {
    faceRectangleService.hideAll()
  }
}

function isInsideStageCoordinates () {
  const stage = storeService.get('stage')
  const actualSizesAndPosition = getActualSizesAndPosition()
  const coordinates = stage.getPointerPosition()

  if (
    coordinates &&
    (
      (coordinates.x < actualSizesAndPosition.x) ||
      (coordinates.x > actualSizesAndPosition.x + actualSizesAndPosition.width) ||
      (coordinates.y < actualSizesAndPosition.y) ||
      (coordinates.y > actualSizesAndPosition.y + actualSizesAndPosition.height)
    )
  ) {
    return false
  }

  return true
}

function init (params = {}, containerId = 'stage') {
  return new Promise(resolve => {
    const $workspace = storeService.get('$workspace')

    // Creating stage
    const stage = new Konva.Stage({
      container: containerId,
      width: $workspace.offsetWidth,
      height: $workspace.offsetHeight
    })
    storeService.set('stage', stage)
    calculateActualSizes(params.width || getDefaultStageSizes().width, params.height || getDefaultStageSizes().height)

    const outsideClickHandler = () => {
      // Deactivate active item if clicked outside the clipping area
      if (isInsideStageCoordinates()) {
        faceRectangleService.showAll()
      } else {
        handleOutsideClick()
        faceRectangleService.hideAll()
      }
    }

    stage
      .on('click', () => outsideClickHandler())
      .on('touchstart', () => outsideClickHandler())
      .on('mousemove', () => {
        if (isInsideStageCoordinates()) {
          faceRectangleService.showAll()
        } else {
          faceRectangleService.hideAll()
        }
      })

    // Creating default background
    const backgroundColor = params.backgroundColor ? params.backgroundColor : config.defaultBackgroundColor
    backgroundService.initDefaultBackground(backgroundColor).then(() => {
      // Creating layer for items
      const layer = createLayer({
        id: ITEMS_LAYER_ID,
        name: ITEMS_LAYER_ID
      })
      stage.add(layer)

      if (!EVENTS_SET) {
        handleEvents()

        EVENTS_SET = true
      }

      resolve()
    })
  })
}

function createClippingMask () {
  const stage = storeService.get('stage')
  const actualSizesAndPosition = getActualSizesAndPosition()
  const maskColor = 'black'
  const layer = new Konva.Layer({
    id: DRAGGING_IMAGE_LAYER_ID
  })
  const clippingGroup = new Konva.Group({
    width: stage.width(),
    height: stage.height(),
    opacity: 0.8
  })

  // Top rectangular
  clippingGroup.add(new Konva.Rect({
    fill: maskColor,
    width: stage.width(),
    height: actualSizesAndPosition.y
  }))

  // Bottom rectangular
  clippingGroup.add(new Konva.Rect({
    fill: maskColor,
    width: stage.width(),
    height: stage.height() - actualSizesAndPosition.y - actualSizesAndPosition.height,
    y: actualSizesAndPosition.height + actualSizesAndPosition.y
  }))

  // Left rectangular
  clippingGroup.add(new Konva.Rect({
    fill: maskColor,
    width: actualSizesAndPosition.x,
    height: actualSizesAndPosition.height,
    y: actualSizesAndPosition.y
  }))

  // Right rectangular
  clippingGroup.add(new Konva.Rect({
    fill: maskColor,
    width: stage.width() - actualSizesAndPosition.x + actualSizesAndPosition.width,
    height: actualSizesAndPosition.height,
    x: actualSizesAndPosition.x + actualSizesAndPosition.width,
    y: actualSizesAndPosition.y
  }))
  layer.add(clippingGroup)

  stage.findOne(`#${ITEMS_LAYER_ID}`).findOne(`.${CLIPPING_GROUP_NAME}`).clip({
    x: undefined,
    y: undefined,
    width: undefined,
    height: undefined
  })

  stage.add(layer)
  stage.draw()
}

function deleteClippingMask () {
  const stage = storeService.get('stage')
  stage.findOne(`#${ITEMS_LAYER_ID}`).findOne(`.${CLIPPING_GROUP_NAME}`).clip(getActualSizesAndPosition())
  stage.find(`#${DRAGGING_IMAGE_LAYER_ID}`).destroy()
}

function calculateActualSizes (initialWidth, initialHeight) {
  const store = storeService.get('vuex')
  initialWidth = initialWidth || store.state.editor.stage.width
  initialHeight = initialHeight || store.state.editor.stage.height
  const leftAndRightMargins = store.state.editor.isMobile ? 0 : config.stage.margin.left * 2
  const topAndBottomMargins = store.state.editor.isMobile ? 0 : config.stage.margin.top * 2
  const workspaceWidth = storeService.get('$workspace').offsetWidth - leftAndRightMargins
  const workspaceHeight = storeService.get('$workspace').offsetHeight - topAndBottomMargins
  const isStageLandscape = initialWidth / initialHeight > 1
  const widthRatio = workspaceWidth / initialWidth
  const heightRatio = workspaceHeight / initialHeight
  let width, height

  if (isStageLandscape) {
    // Try scale by width
    width = workspaceWidth
    height = initialHeight * widthRatio
    if (height > workspaceHeight) {
    // Scaling by width is too big, scaling by height
      height = workspaceHeight
      width = initialWidth * heightRatio
    }
  } else {
    // Try scale by height
    height = workspaceHeight
    width = initialWidth * heightRatio
    if (width > workspaceWidth) {
    // Scaling by height is too big, scaling by width
      width = workspaceWidth
      height = initialHeight * widthRatio
    }
  }

  const actualSizesAndPosition = {
    x: (storeService.get('$workspace').offsetWidth - width) / 2,
    y: (storeService.get('$workspace').offsetHeight - height) / 2,
    width,
    height
  }

  store.dispatch('stageUpdated', {
    width: width,
    height: height,
    realWidth: initialWidth,
    realHeight: initialHeight,
    actualSizesAndPosition
  })
}

function getActualSizesAndPosition () {
  return storeService.get('vuex').state.editor.stage.actualSizesAndPosition
}

/*
 * Create clipping group
 */
function getClippingGroup () {
  const group = new Konva.Group({
    name: CLIPPING_GROUP_NAME
  })

  const actualSizesAndPosition = getActualSizesAndPosition()
  if (actualSizesAndPosition) {
    group.clip({
      height: Math.ceil(actualSizesAndPosition.height),
      width: Math.ceil(actualSizesAndPosition.width),
      x: Math.ceil(actualSizesAndPosition.x),
      y: Math.ceil(actualSizesAndPosition.y)
    })
  }

  return group
}

// Adding clipping group to every layer
function createLayer (params) {
  const layer = new Konva.Layer(params)
  layer.add(getClippingGroup())
  return layer
}

// Adding nodes to the clipping group, not to the layer
// directly so nodes would clip with others too
function addToLayer (layer, node) {
  const clippingGroup = layer.findOne(`.${CLIPPING_GROUP_NAME}`)
  clippingGroup.add(node)
}

function resize (width, height, isRecalculateItems = true, force = false) {
  width = parseInt(width)
  height = parseInt(height)
  // Check if sizes changed
  if (
    !force &&
    storeService.get('vuex').state.editor.stage.realWidth === width &&
    storeService.get('vuex').state.editor.stage.realHeight === height
  ) {
    return
  }

  const stage = storeService.get('stage')

  // Get previous sizes
  const previousSizesAndPosition = getActualSizesAndPosition()

  // Resize all clipping masks
  const clippingGroups = stage.find(`.${CLIPPING_GROUP_NAME}`)
  calculateActualSizes(width, height)
  const actualSizesAndPosition = getActualSizesAndPosition()
  clippingGroups.forEach(clippingGroup => {
    clippingGroup.clip(actualSizesAndPosition)
  })

  // Resize default background
  const defaultBackground = storeService.get('defaultBackground')
  defaultBackground.width(stage.width())
  defaultBackground.height(stage.height())

  if (isRecalculateItems) {
    itemsService.recalculateItems(previousSizesAndPosition)
  }

  stage.draw()

  return actualSizesAndPosition.width / previousSizesAndPosition.width
}

function scale () {
  const stage = storeService.get('stage')
  const workspaceWidth = storeService.get('$workspace').offsetWidth
  const workspaceHeight = storeService.get('$workspace').offsetHeight
  const previousSizesAndPosition = getActualSizesAndPosition()
  storeService.get('stage').width(workspaceWidth)
  storeService.get('stage').height(workspaceHeight)
  const newScale = resize(
    storeService.get('vuex').state.editor.stage.realWidth,
    storeService.get('vuex').state.editor.stage.realHeight,
    false,
    true
  )

  // Scale items' sizes and position
  stage
    .find(node => {
      return node.getAttrs().objectData &&
      [
        itemsService.TYPE_MODEL,
        itemsService.TYPE_OBJECT,
        itemsService.TYPE_TEXT,
        itemsService.TYPE_BACKGROUND,
        itemsService.TYPE_UPLOAD,
        itemsService.TYPE_UPLOAD_BACKGROUND
      ].includes(node.getAttrs().objectData.type)
    })
    .forEach(item => {
      itemsService.scaleItem(item, newScale, previousSizesAndPosition)
    })
  storeService.get('stage').findOne(`#${ITEMS_LAYER_ID}`).draw()

  // Hide tools and other
  itemsService.hideHelpers()
}

function scaleUp (scale, stage) {
  stage = stage || storeService.get('stage')
  stage.width(stage.width() * scale)
  stage.height(stage.height() * scale)
  stage.scale({ x: scale, y: scale })
  stage.draw()
}

function scaleDown (scale, stage) {
  stage = stage || storeService.get('stage')
  stage.width(stage.width() / scale)
  stage.height(stage.height() / scale)
  stage.scale({ x: Math.abs(stage.scaleX()) / scale, y: Math.abs(stage.scaleY()) / scale })
  stage.draw()
}

function cropToClippingArea (stage) {
  // Crop stage to clipping area
  const actualSizesAndPosition = getActualSizesAndPosition()
  const clippingGroups = stage.find(`.${CLIPPING_GROUP_NAME}`)
  clippingGroups.forEach(clippingGroup => {
    clippingGroup.clip({ x: undefined, y: undefined, width: undefined, height: undefined })
    clippingGroup.y(clippingGroup.y() - actualSizesAndPosition.y)
    clippingGroup.x(clippingGroup.x() - actualSizesAndPosition.x)
    stage.draw()
  })
  stage.width(actualSizesAndPosition.width)
  stage.height(actualSizesAndPosition.height)
}

/*
 * Restore stage to previous sizes
 */
function restoreStageFromClippingArea (stage) {
  const actualSizesAndPosition = getActualSizesAndPosition()
  stage.width(storeService.get('$workspace').offsetWidth)
  stage.height(storeService.get('$workspace').offsetHeight)
  const clippingGroups = stage.find(`.${CLIPPING_GROUP_NAME}`)
  clippingGroups.forEach(clippingGroup => {
    clippingGroup.clip({ x: actualSizesAndPosition.x, y: actualSizesAndPosition.y, width: actualSizesAndPosition.width, height: actualSizesAndPosition.height })
    clippingGroup.y(clippingGroup.y() + actualSizesAndPosition.y)
    clippingGroup.x(clippingGroup.x() + actualSizesAndPosition.x)
  })
}

function download (greaterSideWidth = null) {
  return getBlob(greaterSideWidth)
    .then(blob => {
      fileSaver.saveAs(blob, 'download.png')
    })
}

function getBlob (greaterSideWidth = null, mimeType = 'image/png', quality = null) {
  return new Promise((resolve, reject) => {
    try {
      const actualSizesAndPosition = getActualSizesAndPosition()

      // Get scale depends on real stage size or max side width
      let scale
      if (greaterSideWidth) {
        scale = greaterSideWidth / actualSizesAndPosition.width
      } else {
        scale = storeService.get('vuex').state.editor.stage.realWidth / actualSizesAndPosition.width
      }

      // Create stage duplicate
      const stageClone = storeService.get('stage').clone()

      // Crop stage to clipping area
      cropToClippingArea(stageClone)

      // Move default background to items layer
      const defaultBackground = storeService.get('defaultBackground').clone()
      const itemsLayer = stageClone.findOne(`.${ITEMS_LAYER_ID}`)
      itemsLayer.add(defaultBackground)
      defaultBackground.moveToBottom()

      // Add items layer clone to hidden stage
      const hiddenStage = createHiddenDulplicate({ width: itemsLayer.width(), height: itemsLayer.height() })
      hiddenStage.add(itemsLayer)

      // Make all images opaque
      setLayerItemsOpaque(itemsLayer)

      itemsLayer.draw()

      // If current stage real width is more than current stage width (scale > 1)
      // then scale the blobing stage according to the real width
      if (scale !== 1) {
        scaleUp(scale, hiddenStage)
      }

      const callback = blob => {
        hiddenStage.container().remove()
        resolve(blob)
      }
      hiddenStage.container().querySelector('canvas').toBlob(blob => callback(blob), mimeType, quality)
    } catch (error) {
      console.log(`CREATOR ERROR: %c ${error}`, 'color: red')
      reject(error.message)
    }
  })
}

function hexToRgba (hex, alpha = 1) {
  // If rgba already
  if (/^rgba/.test(hex)) {
    return hex
  }

  // Check for shortcuts
  if (hex.slice(1).length === 3) {
    hex = `#${hex[1]}${hex[1]}${hex[2]}${hex[2]}${hex[3]}${hex[3]}`
  }

  const r = parseInt(hex.slice(1, 3), 16)
  const g = parseInt(hex.slice(3, 5), 16)
  const b = parseInt(hex.slice(5, 7), 16)

  return `rgba(${r},${g},${b},${alpha})`
}

function getDefaultStageSizes () {
  return {
    width: config.stage.sizesPresets[config.stage.defaultSizePreset].width,
    height: config.stage.sizesPresets[config.stage.defaultSizePreset].height
  }
}

/**
 * Make base64 url of stage preview
 * @param {} activeItem
 */
function getStagePreview (activeItem) {
  const stageClone = storeService.get('stage').clone()

  // Make all images opaque
  setLayerItemsOpaque(stageClone)

  // If active item defined - make it bright, other items more transparent (opacity=0.*)
  if (activeItem) {
    // Delete first layer with default background
    stageClone.getLayers()[0].destroy()

    // Move default background to items layer
    const defaultBackground = storeService.get('defaultBackground').clone()
    defaultBackground.moveTo(stageClone.getLayers()[0])
    defaultBackground.moveToBottom()
    defaultBackground.opacity(0.2)

    // Make items transparent
    storeService.get('items').forEach(item => {
      stageClone.find(itemClone => {
        if (
          itemClone.getAttrs().objectData &&
            itemClone.getAttrs().objectData.id === item.getAttrs().objectData.id &&
            itemClone.getAttrs().objectData.id !== activeItem.getAttrs().objectData.id
        ) {
          itemClone.opacity(0.2)
        }
      })
    })
    stageClone.draw()
  }

  cropToClippingArea(stageClone)
  const dataUrl = stageClone.toDataURL({ pixelRatio: 1 })
  stageClone.destroy()

  return dataUrl
}

function createHiddenDulplicate (params = {}) {
  // Create DOM element for stage clone
  const $stageDuplicate = document.createElement('div')
  $stageDuplicate.style.display = 'none'
  storeService.get('$workspace').appendChild($stageDuplicate)

  // Create stage clone
  params.container = $stageDuplicate
  return new Konva.Stage(params)
}

function rotate () {
  const store = storeService.get('vuex')
  if (store.state.editor.isPortrait) {
    const currentStageRealWidth = storeService.get('vuex').state.editor.stage.realWidth
    const currentStageRealHeight = storeService.get('vuex').state.editor.stage.realHeight
    storeService.get('commandManager').execute(new ResizeStageCommand(currentStageRealHeight, currentStageRealWidth))
  } else {
    const currentStageRealWidth = storeService.get('vuex').state.editor.stage.realWidth
    const currentStageRealHeight = storeService.get('vuex').state.editor.stage.realHeight
    storeService.get('commandManager').execute(new ResizeStageCommand(currentStageRealHeight, currentStageRealWidth))
  }
}

/*
 * Make all images opaque
 */
function setLayerItemsOpaque (container) {
  container.find(item => {
    if (item.getAttrs().opacity && item.getAttrs().opacity !== 1) {
      item.opacity(1)
    }
  })
  container.find('.loader').destroy()
}

export default {
  init,
  rotate,
  getStagePreview,
  getDefaultStageSizes,
  hexToRgba,
  getBlob,
  getActualSizesAndPosition,
  download,
  calculateRotationAngle,
  addToLayer,
  createLayer,
  handleEvents,
  scale,
  resize,
  scaleUp,
  scaleDown,
  cropToClippingArea,
  restoreStageFromClippingArea,
  createClippingMask,
  handleOutsideClick,
  deleteClippingMask,
  CLIPPING_GROUP_NAME,
  ITEMS_LAYER_ID,
  TYPE_TEXT_COLOR_PICKER
}
