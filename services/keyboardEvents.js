const ENTER = 'ENTER'
const ESCAPE = 'ESCAPE'
const DELETE = 'DELETE'
const BACKSPACE = 'BACKSPACE'
const ARROW_UP = 'ARROW_UP'
const ARROW_DOWN = 'ARROW_DOWN'
const ARROW_LEFT = 'ARROW_LEFT'
const ARROW_RIGHT = 'ARROW_RIGHT'
const Z = 'Z'

const keyboardEvents = {
  [Z]: {
    keyCode: 90,
    keys: ['z']
  },
  [ENTER]: {
    keyCode: 13,
    keys: ['Enter']
  },
  [ESCAPE]: {
    keyCode: 27,
    keys: ['Esc', 'Escape']
  },
  [BACKSPACE]: {
    keyCode: 8,
    keys: ['Backspace']
  },
  [DELETE]: {
    keyCode: 46,
    keys: ['Delete']
  },
  [ARROW_UP]: {
    keyCode: 38,
    keys: ['ArrowUp']
  },
  [ARROW_DOWN]: {
    keyCode: 40,
    keys: ['ArrowDown']
  },
  [ARROW_LEFT]: {
    keyCode: 37,
    keys: ['ArrowLeft']
  },
  [ARROW_RIGHT]: {
    keyCode: 39,
    keys: ['ArrowRight']
  }
}

function is (event, keyNames, additionalKey = true) {
  if (!Array.isArray(keyNames)) {
    keyNames = [keyNames]
  }

  let match = false
  keyNames.forEach(keyName => {
    if (
      additionalKey &&
      keyboardEvents[keyName] &&
      (
        (event.key && keyboardEvents[keyName].keys.includes(event.key)) ||
        keyboardEvents[keyName].keyCode === event.keyCode
      )
    ) {
      match = true
    }
  })

  return match
}

export default {
  is,
  Z,
  ENTER,
  ESCAPE,
  DELETE,
  BACKSPACE,
  ARROW_UP,
  ARROW_DOWN,
  ARROW_LEFT,
  ARROW_RIGHT
}
