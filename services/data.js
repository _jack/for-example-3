import storeService from './store'
import dataService from './data'
import stageService from './stage'
import itemsService from './items/common'
import backgroundService from './items/background'
import textService from './items/text'
import photosApi from '../api/photos'
import config from '~/config'
import AddItemCommand from '~/history/commands/addItem'

const nanoid = require('nanoid')

const STATUS_UPLOADING = 'uploading'
const STATUS_SUCCESS = 'success'
const STATUS_ERROR = 'error'

const DISAPPEAR_TIME = 4000
const TEMP_UPLOADING = '-temp-uploading'

const AUTOSAVE_STATUS_SAVING = 'AUTOSAVE_STATUS_SAVING'
const AUTOSAVE_STATUS_SAVED = 'AUTOSAVE_STATUS_SAVED'
const AUTOSAVE_STATUS_ERROR = 'AUTOSAVE_STATUS_ERROR'

function getStageData (full = false, greaterSideWidth = null) {
  const stage = storeService.get('stage')
  const itemsLayer = stage.findOne(`#${stageService.ITEMS_LAYER_ID}`).clone()
  const konvaJson = JSON.parse(itemsLayer.toJSON())
  const scale = storeService.get('vuex').state.editor.stage.realWidth / storeService.get('vuex').state.editor.stage.width

  let multiplier = 1
  if (greaterSideWidth) {
    const stageParams = storeService.get('vuex').state.editor.stage
    const currentGreaterSideWidth = stageParams.realWidth > stageParams.realHeight ? stageParams.realWidth : stageParams.realHeight
    multiplier = greaterSideWidth / currentGreaterSideWidth
  }

  const stageData = {
    stage: {
      width: Math.round(storeService.get('vuex').state.editor.stage.realWidth * multiplier),
      height: Math.round(storeService.get('vuex').state.editor.stage.realHeight * multiplier),
      backgroundColor: stageService.hexToRgba(storeService.get('defaultBackground').fill())
    },
    items: []
  }

  const clippingMaskAttributes = konvaJson.children[0].attrs
  konvaJson.children[0].children.forEach((item, index) => {
    // TODO: Better temp detection
    // Skip 'temp-uploading' images (they can't be saved or they'll break api)
    if (item.attrs.objectData && item.attrs.objectData.id.indexOf(TEMP_UPLOADING) !== -1) return

    const itemType = item.attrs.objectData ? item.attrs.objectData.type : undefined
    const isItem = [
      itemsService.TYPE_OBJECT,
      itemsService.TYPE_MODEL,
      itemsService.TYPE_BACKGROUND,
      itemsService.TYPE_TEXT,
      itemsService.TYPE_UPLOAD,
      itemsService.TYPE_UPLOAD_BACKGROUND
    ].includes(itemType)
    if (isItem) {
      let itemImage = item
      const element = stage.findOne(`#${stageService.ITEMS_LAYER_ID}`).findOne(node => node.attrs.objectData && node.attrs.objectData.id === item.attrs.objectData.id)
      if ([itemsService.TYPE_MODEL, itemsService.TYPE_OBJECT, itemsService.TYPE_BACKGROUND, itemsService.TYPE_UPLOAD, itemsService.TYPE_UPLOAD_BACKGROUND].includes(item.attrs.objectData.type)) {
        itemImage = item.children.find(child => child.attrs.name === 'itemImage')
      }
      let itemWidth = item.attrs.width * (item.attrs.scaleX || 1)
      let width = itemImage.attrs.width * (item.attrs.scaleX || 1)
      let height
      const x = Math.round((item.attrs.x - Math.abs(itemWidth) / 2 - clippingMaskAttributes.clipX) * scale)
      width *= scale

      const itemData = {
        width: Math.round(width < 0 ? Math.abs(width) : width),
        x: x,
        index,
        rotation: item.attrs.rotation ? stageService.calculateRotationAngle(item.attrs.rotation) : 0,
        type: itemType
      }

      if (itemType === itemsService.TYPE_TEXT) {
        height = element.getHeight()

        // Make png encoded by base64 from text
        itemData.base64 = textService.makePng(element, scale)

        // Generate text properties
        itemData.textProperties = {
          id: item.attrs.objectData.id,
          align: element.align(),
          fontSize: Math.round(element.fontSize() * scale * 0.88),
          fontStyle: element.fontStyle(),
          text: element.text(),
          fontFamily: element.fontFamily(),
          fill: element.fill()
        }
      } else {
        height = itemImage.attrs.height * (item.attrs.scaleY || 1)

        if ([itemsService.TYPE_UPLOAD, itemsService.TYPE_UPLOAD_BACKGROUND].includes(itemType)) {
          itemData.uploadData.id = item.attrs.objectData.id
          itemData.uploadData.background = item.attrs.objectData.background
        } else if (full) {
          itemData.photoData = filterObjectData(item.attrs.objectData)
        } else {
          itemData.photoData = {
            id: item.attrs.objectData.id,
            photoId: item.attrs.objectData.photoId
          }
        }
      }

      // Handle swapped face
      const currentSwappedFace = item.attrs.objectData.currentSwappedFace
      if (currentSwappedFace) {
        itemData.swappedFace = {
          id: currentSwappedFace.id
        }
      }

      // Set correct type for saving
      if (itemType === itemsService.TYPE_UPLOAD_BACKGROUND) {
        itemData.type = itemsService.TYPE_UPLOAD
      }

      // Handle object's shadow if exists
      if (itemsService.SHADOWED_TYPES.includes(itemType) && item.attrs.shadowData) {
        const itemShadow = element.findOne(child => child.attrs.name === 'itemShadow')
        itemData.ignoreShadow = !itemShadow.attrs.opacity
      }

      itemData.width = Math.round(itemData.width * multiplier)
      itemData.height = Math.round((height < 0 ? Math.abs(height) : height) * scale * multiplier)
      itemData.horizontalMirror = item.attrs.scaleX < 0
      itemData.verticalMirror = item.attrs.scaleY < 0
      itemData.x = Math.round(itemData.x * multiplier)
      itemData.y = Math.round((item.attrs.y - Math.abs(height) / 2 - clippingMaskAttributes.clipY) * scale * multiplier)
      stageData.items.push(itemData)
    }
  })

  itemsLayer.destroy()

  return stageData
}

function restore (data, stageContainerId = 'stage') {
  return new Promise((resolve, reject) => {
    if (!data.stage) {
      return reject('Stage data not found')
    }

    // Destroy current stage if exists
    if (storeService.get('stage')) {
      storeService.destroyStage()
    }

    let handleItems = (resolve) => {
      let promises = []
      // Adding models, texts, objects and other stuff to the stage
      if (data.items) {
        const scale = storeService.get('vuex').state.editor.stage.width / data.stage.width
        data.items.forEach(item => {
          switch (item.type) {
            case itemsService.TYPE_OBJECT:
            case itemsService.TYPE_MODEL:
              promises.push(new Promise(resolve => {
                updateAssetData(item).then(assetData => {
                  return itemsService.handleImage(
                    assetData,
                    itemsService.getMainAttributes(getRecalculatedAttributes(item, scale)),
                    false,
                    true
                  )
                    .then(() => resolve())
                })
              }))
              break
            case itemsService.TYPE_UPLOAD_BACKGROUND:
            case itemsService.TYPE_BACKGROUND:
              promises.push(new Promise(resolve => {
                updateAssetData(item).then(assetData => {
                  return backgroundService.handle(
                    assetData,
                    itemsService.getMainAttributes(getRecalculatedAttributes(item, scale)),
                    true
                  )
                    .then(() => resolve())
                })
              }))
              break
            case itemsService.TYPE_TEXT:
              const attributes = itemsService.getMainAttributes(
                getRecalculatedAttributes(Object.assign({}, item, item.textProperties), scale)
              )
              attributes.fontSize *= scale
              attributes.fontSize /= 0.88
              promises.push(textService.handle(Object.assign({}, attributes, { id: item.textProperties.id })))
              break
            case itemsService.TYPE_UPLOAD:
              promises.push(new Promise(resolve => {
                updateAssetData(item).then(assetData => {
                  if (assetData.type === itemsService.TYPE_UPLOAD_BACKGROUND) {
                    return backgroundService.handle(
                      assetData,
                      itemsService.getMainAttributes(getRecalculatedAttributes(item, scale)),
                      true
                    )
                      .then(() => resolve())
                  } else {
                    return itemsService.handleImage(
                      assetData,
                      itemsService.getMainAttributes(getRecalculatedAttributes(item, scale)),
                      false,
                      true
                    )
                      .then(() => resolve())
                  }
                })
              }))
              break
          }
        })
      }

      return Promise.all(promises).then(() => {
        // Set z-indexs
        const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
        data.items.forEach((itemData, index) => {
          let item
          switch (itemData.type) {
            case itemsService.TYPE_OBJECT:
            case itemsService.TYPE_MODEL:
            case itemsService.TYPE_BACKGROUND:
              item = itemsLayer.findOne(node => {
                return node.attrs.objectData && node.attrs.objectData.id === itemData.photoData.id
              })
              break
            case itemsService.TYPE_UPLOAD_BACKGROUND:
            case itemsService.TYPE_UPLOAD:
              item = itemsLayer.findOne(node => {
                return node.attrs.objectData && node.attrs.objectData.id === itemData.uploadData.id
              })
              break
            case itemsService.TYPE_TEXT:
              item = itemsLayer.findOne(node => {
                return node.attrs.objectData && node.attrs.objectData.id === itemData.textProperties.id
              })
              break
          }
          if (item) {
            item.moveToTop()
          }
        })
        itemsLayer.draw()

        resolve()
      })
    }

    stageService.init(data.stage, stageContainerId).then(() => handleItems(resolve))
  })
}

/**
 * Update asset data by its identificator
 */
function updateAssetData (item) {
  return new Promise(resolve => {
    let id
    if ([itemsService.TYPE_UPLOAD, itemsService.TYPE_UPLOAD_BACKGROUND].includes(item.type)) id = item.uploadData.id
    else id = item.photoData.id
    photosApi.assetData(item.type, id)
      .then(response => {
        let updatedData = {}
        if (response) {
          let type = item.type
          if (response.data.upload_type === 'background') {
            type = itemsService.TYPE_UPLOAD_BACKGROUND
          }

          response.data.ignoreShadow = item.ignoreShadow
          response.data.verticalMirror = item.verticalMirror
          response.data.horizontalMirror = item.horizontalMirror
          response.data.swappedFace = item.swappedFace
          updatedData = itemsService.handleAssetObject(type, response.data)
        }
        resolve(Object.assign([], item.photoData, updatedData))
      })
  })
}

/**
 * Recalculate x and y according to actual scale
 */
function getRecalculatedAttributes (item, scale) {
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  const params = {
    width: item.width * scale,
    height: item.height * scale,
    x: (item.x + item.width / 2) * scale + actualSizesAndPosition.x,
    y: (item.y + item.height / 2) * scale + actualSizesAndPosition.y
  }

  return Object.assign({}, item, params)
}

function getSnapshotData (type) {
  const stageClone = storeService.get('stage').clone()
  stageService.cropToClippingArea(stageClone)

  const snapshotData = {
    id: nanoid(),
    data: dataService.getStageData(true),
    dataUrl: stageClone.toDataURL({ pixelRatio: 1 }),
    type,
    time: new Date().getTime()
  }

  stageClone.destroy()

  return snapshotData
}

function makeSnapshot (type) {
  const snapshotData = getSnapshotData(type)
  storeService.get('vuex').dispatch('saveSnapshot', snapshotData)

  return snapshotData.data
}

function filterObjectData (objectData) {
  return {
    id: objectData.id,
    type: objectData.type,
    modelPoints: objectData.modelPoints,
    objectPoints: objectData.objectPoints,
    point: objectData.point,
    assetWidth: objectData.assetWidth,
    assetHeight: objectData.assetHeight,
    width: objectData.width,
    height: objectData.height,
    url: objectData.url,
    photoId: objectData.photoId,
    thumb: objectData.thumb,
    title: objectData.title,
    shadowData: objectData.shadowData
  }
}

function handleCollageSaving (autoSave = false) {
  const store = storeService.get('vuex')
  let currentTimer = store.state.editor.autosave.timer

  if (autoSave) {
    // If already timer - clear it to create a new one
    if (currentTimer) {
      clearAutosaveTimeout(currentTimer)
    }

    return new Promise(resolve => {
      currentTimer = setTimeout(() => {
        clearAutosaveTimeout(currentTimer)
        resolve(saveCollage(autoSave))
      }, 1000)
      store.commit('setAutosaveTimer', currentTimer)
    })
  } else {
    clearAutosaveTimeout(currentTimer)
    return saveCollage(autoSave)
  }
}

function saveCollage (autoSave) {
  const store = storeService.get('vuex')
  const userCollage = store.state.editor.userCollage

  storeService.get('vuex').dispatch('setAutosaveStatus', AUTOSAVE_STATUS_SAVING)

  // Find current collage in user collages
  // It might be collage of someone else opened -
  // then it should by saved as a new one of current user
  let userCollageId
  if (userCollage) {
    const currentUserCollage = store.state.editor.userCollages.items.find(collage => collage.id === userCollage.id)
    if (currentUserCollage) {
      userCollageId = userCollage.id
    }
  }

  return photosApi.saveUserCollage({ collage: dataService.getStageData() }, userCollageId)
    .then(collageData => {
      storeService.get('vuex').dispatch('setAutosaveStatus', AUTOSAVE_STATUS_SAVED)

      const collage = dataService.handleCollage(collageData)
      if (!userCollageId) {
        const items = [collage, ...store.state.editor.userCollages.items]
        const total = store.state.editor.userCollages.total + 1
        storeService.get('vuex').dispatch('setUserCollages', { items, total, page: 1 })
        history.replaceState({}, null, encodeURI(`/creator/photo/${collageData.id}`))
        store.dispatch('userCollageSet', collage)
      }

      // Save collage preview
      return stageService.getBlob(500, 'image/jpeg', 0.9)
        .then(blob => {
          const formData = new FormData()
          formData.append('file', blob)
          return photosApi.saveCollagePreview(formData, collageData.id)
            .then(collagePreview => {
              const image = new Image()
              image.src = collagePreview.url
              store.dispatch('updateCollage', {
                id: collage.id,
                preview: collagePreview.url
              })
            })
        })
    })
    .catch(() => {
      storeService.get('vuex').dispatch('setAutosaveStatus', AUTOSAVE_STATUS_ERROR)
      storeService.get('vuex').dispatch('errorsSet', { api: 'Oops... Save failed.' })
    })
}

function clearAutosaveTimeout (timeout) {
  clearTimeout(timeout)
  storeService.get('vuex').commit('setAutosaveTimer', null)
}

function duplicateCollage (collage, changeUrl = true) {
  return photosApi.duplicateUserCollage(collage)
    .then(collageData => {
      const store = storeService.get('vuex')
      const collage = handleCollage(collageData)
      const userCollages = [collage, ...store.state.editor.userCollages.items]
      const userCollagesTotal = store.state.editor.userCollages.total + 1
      storeService.get('vuex').dispatch('setUserCollages', { items: userCollages, total: userCollagesTotal, page: 1 })
      store.dispatch('userCollageSet', collage)

      if (changeUrl) {
        history.replaceState({}, null, encodeURI(`/creator/photo/${collage.id}`))
      }
      return collage
    })
}

function getCollages (params) {
  return photosApi.getUserCollages(params)
    .then(collagesData => {
      let userCollages = []
      collagesData.collages.forEach(collage => {
        userCollages.push(dataService.handleCollage(collage))
      })
      storeService.get('vuex').dispatch('setUserCollages', { items: userCollages, total: collagesData.total, page: 1 })
      return userCollages
    })
}

function getUser () {
  // Handle user session hash
  let userSessionHash
  if (typeof window !== 'undefined') {
    userSessionHash = window.localStorage.getItem('editorUserSessionHash')
    if (!userSessionHash) {
      userSessionHash = require('nanoid')()
    }
    window.localStorage.setItem('editorUserSessionHash', userSessionHash)
  }

  return photosApi.user({ guest_id: userSessionHash })
    .then(user => {
      const store = storeService.get('vuex')
      store.dispatch('userSet', user)

      if (user && user.roles.find(role => role === 'user')) {
        updateLicenseInfo()
      }

      return user
    })
}

function updateLicenseInfo () {
  const store = storeService.get('vuex')
  if (store.state.editor.user && (!store.state.editor.user.license || (store.state.editor.user.license && store.state.editor.user.license.expired))) {
    store.dispatch('fillUserInfo')
      .then(() => {
        let lastLicense = 0

        // Find most later license
        if (Array.isArray(store.state.auth.user.licenses)) {
          store.state.auth.user.licenses.forEach(license => {
            if (license.expire > lastLicense) {
              lastLicense = license.expire
            }
          })
        }

        // If any active license - update photo user info
        if (lastLicense * 1000 > Date.now()) {
          photosApi.user({ sync: 1 })
            .then(user => {
              store.dispatch('userSet', user)
              store.dispatch('setLicenseVerified', true)
            })
        } else {
          store.dispatch('setLicenseVerified', true)
        }
      })
  } else {
    store.dispatch('setLicenseVerified', true)
  }
}

function getSeo (collage) {
  return {
    title: 'Moose - Photo Creator',
    description: 'Don\'t search for stock photos. Create them.',
    googleTitle: 'Photo Creator',
    googleDescription: 'A tool for creating stock photo scenes',
    image: collage ? `${collage.preview && collage.preview.url}` : 'https://photos.icons8.com/vue-static/photos/creator-share.png',
    url: `${process.env.host}/creator${collage ? `/photo/${collage.id}` : ''}`
  }
}

function handleCollage (collage) {
  return {
    id: collage.id,
    preview: collage.preview,
    created: collage.createdAt,
    updated: collage.updatedAt,
    isExported: collage.exported,
    isDeleting: false,
    isDeleted: false,
    isDuplicating: false,
    deletingTimer: null,
    data: JSON.parse(collage.json).collage
  }
}

// TODO: Allow user to re-upload
// TODO: Rethink and refactor this section.
function handleUploads (files, event) {
  let incorrectType = []
  let incorrectSize = []

  let isBackgroundUpdated = false

  Array.from(files).forEach(file => {
    if (!isCorrectType(file)) {
      incorrectType.push(file.name)
      return
    }
    if (!isCorrectSize(file)) {
      incorrectSize.push(file.name)
      return
    }

    let uploadingId = nanoid() + TEMP_UPLOADING
    storeService.get('vuex').dispatch('addCurrentUpload', { src: URL.createObjectURL(file), uploadingId, uploadingStatus: STATUS_UPLOADING })

    // Adding preview before uploading. Not calling a Command so it isn't added to the history.
    getSizeAndTypeOfImage(file).then((res) => {
      let imgData = storeService.get('vuex').state.editor.currentUploads[storeService.get('vuex').state.editor.currentUploads.findIndex(x => x.uploadingId === uploadingId)]
      imgData = { ...imgData, ...res, id: uploadingId, thumb: imgData.src }
      let params = {}
      if (event && imgData.type !== itemsService.TYPE_UPLOAD_BACKGROUND) {
        params.x = event.clientX - storeService.get('$artBoard').offsetLeft
        params.y = event.clientY - config.toolsHeight
      }
      if (imgData.type === itemsService.TYPE_UPLOAD_BACKGROUND) {
        if (!isBackgroundUpdated) {
          backgroundService.handle(imgData, params)
          isBackgroundUpdated = true
        }
      } else {
        itemsService.handleImage(imgData, params)
      }
    })

    let data = new FormData()
    data.append('file', file)

    photosApi.uploadImage(data)
      .then(res => {
        const type = res.upload_type === 'background' ? itemsService.TYPE_UPLOAD_BACKGROUND : itemsService.TYPE_UPLOAD
        storeService.get('vuex').dispatch('updateCurrentUpload',
          {
            uploadingId,
            payload: { ...itemsService.handleAssetObject(type, res), uploadingStatus: STATUS_SUCCESS }
          }
        )
          .then(() => {
            // Adding an actual image to the scene.
            let itemData = storeService.get('vuex').state.editor.currentUploads[storeService.get('vuex').state.editor.currentUploads.findIndex(x => x.uploadingId === uploadingId)]

            // Get actual coordinates since user can move the item while it's loading and remove it afterwards :)
            const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
            let item = itemsLayer.findOne(item => item.getAttrs().objectData && item.getAttrs().objectData.id === uploadingId)

            // TODO: Maybe get item z-index as well?
            let params = {}
            if (item) {
              params.x = item.attrs.x
              params.y = item.attrs.y
            }

            // Separate handling for bg, otherwise it removes current bg.
            // TODO: rewrite this section for better BG handling
            if (item && type === itemsService.TYPE_UPLOAD_BACKGROUND) {
              itemsService.removeItem(item)
              item = null
              storeService.get('commandManager').execute(new AddItemCommand(itemData, params))
            } else if (item) {
              storeService.get('commandManager').execute(new AddItemCommand(itemData, params)).then(() => {
                itemsService.removeItem(item)
              })
            }

            setTimeout(() => storeService.get('vuex').dispatch('updateCurrentUpload',
              {
                uploadingId,
                payload: { uploadingStatus: null }
              }
            ), DISAPPEAR_TIME)
          })
      })
      .catch((error) => {
        console.log('Oops... ' + error)
        storeService.get('vuex').dispatch('updateCurrentUpload',
          {
            uploadingId,
            payload: { uploadingStatus: STATUS_ERROR }
          })

        // TODO: Handle error in a more beautiful way :)
        // Removing preview after uploading. Not calling a command so it isn't added to the history.
        const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
        const item = itemsLayer.findOne(item => item.getAttrs().objectData && item.getAttrs().objectData.id === uploadingId)
        itemsService.removeItem(item)
      })
  })

  storeService.get('vuex')
    .dispatch('errorsSet', { files: { incorrectType, incorrectSize } })
    .then(() => setTimeout(() => storeService.get('vuex').dispatch('errorsSet', { files: { incorrectSize: [], incorrectType: [] } }), 5000))
}

// TODO: Better background detection.
function getSizeAndTypeOfImage (file) {
  return new Promise(resolve => {
    const type = file.type === 'image/jpeg' ? itemsService.TYPE_UPLOAD_BACKGROUND : itemsService.TYPE_UPLOAD

    let imageObj = new Image()
    imageObj.crossOrigin = 'anonymous'
    imageObj.src = URL.createObjectURL(file)

    imageObj.onload = () => {
      const width = imageObj.width
      const height = imageObj.height

      imageObj = null

      resolve({
        type,
        width,
        height
      })
    }
  })
}

function isCorrectType (file) {
  const CORRECT_TYPES = ['image/jpeg', 'image/png']
  return CORRECT_TYPES.includes(file.type)
}

function isCorrectSize (file) {
  const MAX_SIZE = 52428800 // 50 MB in bytes
  return file.size < MAX_SIZE
}

export default {
  handleCollage,
  getSeo,
  getUser,
  getCollages,
  handleCollageSaving,
  duplicateCollage,
  getStageData,
  restore,
  handleUploads,
  STATUS_UPLOADING,
  STATUS_SUCCESS,
  STATUS_ERROR,
  AUTOSAVE_STATUS_SAVING,
  AUTOSAVE_STATUS_SAVED,
  AUTOSAVE_STATUS_ERROR
}
