const SET_ACTIVE = 'SET_ACTIVE'
const SET_ALTERNATIVE = 'SET_ALTERNATIVE'

const state = () => ({
  models: {
    active: 2,
    items: [
      {
        path: require('../../static/vue-static/landing-model-1.png'),
        scenes: [
          'https://photos.icons8.com/creator/photo/5cb440a0f0fed70015948c90',
          'https://photos.icons8.com/creator/photo/5cb442d6f0fed70019948bcf',
          'https://photos.icons8.com/creator/photo/5cb055f2f0fed70018e074a8',
          'https://photos.icons8.com/creator/photo/5cb05b2cf0fed70014e074fe'
        ],
        alternatives: {
          active: null,
          items: [
            require('../../static/vue-static/landing-alternative-1-1.png'),
            require('../../static/vue-static/landing-alternative-1-2.png'),
            require('../../static/vue-static/landing-alternative-1-3.png')
          ]
        }
      },
      {
        path: require('../../static/vue-static/landing-model-2.png'),
        scenes: [
          'https://photos.icons8.com/creator/photo/5cb4414ff0fed70022948bd6',
          'https://photos.icons8.com/creator/photo/5cb4434cf0fed70022948be7',
          'https://photos.icons8.com/creator/photo/5cb04c12f0fed70014e073e1',
          'https://photos.icons8.com/creator/photo/5cb05b65f0fed70014e07502'
        ],
        alternatives: {
          active: null,
          items: [
            require('../../static/vue-static/landing-alternative-2-1.png'),
            require('../../static/vue-static/landing-alternative-2-2.png'),
            require('../../static/vue-static/landing-alternative-2-3.png')
          ]
        }
      },
      {
        path: require('../../static/vue-static/landing-model-3.png'),
        scenes: [
          'https://photos.icons8.com/creator/photo/5cb441bff0fed70015948c99',
          'https://photos.icons8.com/creator/photo/5cb443cbf0fed7001d948c76',
          'https://photos.icons8.com/creator/photo/5cb057f4f0fed7001ee0747d',
          'https://photos.icons8.com/creator/photo/5cb4b912f0fed70027d9d145'
        ],
        alternatives: {
          active: null,
          items: [
            require('../../static/vue-static/landing-alternative-3-1.png'),
            require('../../static/vue-static/landing-alternative-3-2.png'),
            require('../../static/vue-static/landing-alternative-3-3.png')
          ]
        }
      },
      {
        path: require('../../static/vue-static/landing-model-4.png'),
        scenes: [
          'https://photos.icons8.com/creator/photo/5cb44228f0fed7001d948c51',
          'https://photos.icons8.com/creator/photo/5cb44454f0fed70015948cb4',
          'https://photos.icons8.com/creator/photo/5cb05a97f0fed70024e074db',
          'https://photos.icons8.com/creator/photo/5cb05caff0fed70024e074f5'
        ],
        alternatives: {
          active: null,
          items: [
            require('../../static/vue-static/landing-alternative-4-1.png'),
            require('../../static/vue-static/landing-alternative-4-2.png'),
            require('../../static/vue-static/landing-alternative-4-3.png')
          ]
        }
      }
    ]
  },
  backgrounds: {
    active: 0,
    items: [
      require('../../static/vue-static/landing-background-1.jpg'),
      require('../../static/vue-static/landing-background-2.jpg'),
      require('../../static/vue-static/landing-background-3.jpg'),
      require('../../static/vue-static/landing-background-4.jpg')
    ]
  },
  objects: {
    active: 1,
    items: [
      require('../../static/vue-static/landing-object-1.png'),
      require('../../static/vue-static/landing-object-2.png'),
      require('../../static/vue-static/landing-object-3.png'),
      require('../../static/vue-static/landing-object-4.png')
    ]
  },
  borders: {
    active: 2,
    items: [
      require('../../static/vue-static/landing-border-1.png'),
      require('../../static/vue-static/landing-border-2.png'),
      require('../../static/vue-static/landing-border-3.png'),
      require('../../static/vue-static/landing-border-4.png')
    ]
  }
})

const getters = {
  editSceneLink: state => {
    const modelIndex = state.models.active
    const backgroundIndex = state.backgrounds.active

    return state.models.items[modelIndex].scenes[backgroundIndex]
  }
}

const mutations = {
  [SET_ACTIVE] (state, element) {
    const { category, index } = element
    state[category].active = index

    if (category === 'models') {
      state[category].items[index].alternatives.active = null
      state.borders.active = index
    }
  },

  [SET_ALTERNATIVE] (state, element) {
    const { modelIndex, altIndex } = element
    state.models.items[modelIndex].alternatives.active = altIndex
  }
}

const store = () => ({
  strict: process.env.NODE_ENV !== 'production',
  state,
  getters,
  mutations
})

export default store
