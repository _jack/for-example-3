'use strict'

import itemsService from '../../services/items/common'

export default class HideShadowCommand {
  constructor (item) {
    this.item = item
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      itemsService.revealShadow(this.item)
        .then(() => resolve())
    })
  }

  redo () {
    return new Promise(resolve => {
      itemsService.hideShadow(this.item)
        .then(() => resolve())
    })
  }
}
