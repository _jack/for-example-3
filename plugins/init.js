import storeService from '../services/store'
import photosApi from '../api/photos'
import backgroundService from '../services/items/background'
import modelService from '../services/items/model'
import objectService from '../services/items/object'
import uploadService from '../services/items/upload'
import dataService from '../services/data'

export default (data) => {
  storeService.set('vuex', data.store)
  storeService.set('router', data.app.router)
  const store = storeService.get('vuex')

  // Set default action
  store.dispatch('setCurrentAction', 'models')

  // Load User, then load models, backgrounds, object, etc...
  dataService.getUser().then(() => {
    // Backgrounds
    const backgroundsSortingType = backgroundService.SORT_RISING
    store.dispatch('setBackgrounds', { items: [], total: 0, page: 0, sorting: backgroundsSortingType })
    store.dispatch('setSearchBackgrounds', { items: [], total: 0, sorting: backgroundsSortingType })
    photosApi.backgrounds(backgroundService.getSortingParams(backgroundsSortingType))
      .then(response => {
        const backgrounds = backgroundService.handlePhotos(response.backgrounds)
        store.dispatch('setBackgrounds', { items: backgrounds, total: response.total })
      })

    // Objects
    const objectsSortingType = objectService.SORT_RISING
    store.dispatch('setObjects', { items: [], total: 0, page: 0, sorting: objectsSortingType })
    store.dispatch('setSearchObjects', { items: [], total: 0, sorting: objectsSortingType })
    photosApi.objects(objectService.getSortingParams(objectsSortingType))
      .then(response => {
        const objects = objectService.handlePhotos(response.objects)
        store.dispatch('setObjects', { items: objects, total: response.total })
      })

    // Models
    const modelsSortingType = modelService.SORT_RISING
    store.dispatch('setModels', { items: [], total: 0, page: 0, sorting: modelsSortingType })
    store.dispatch('setSearchModels', { items: [], total: 0, sorting: modelsSortingType })
    photosApi.models(modelService.getSortingParams(modelsSortingType))
      .then(response => {
        const models = modelService.handlePhotos(response.models)
        store.dispatch('setModels', { items: models, total: response.total })
      })

    // Uploads
    photosApi.uploads()
      .then(response => {
        const uploads = uploadService.handlePhotos(response.uploads)
        store.dispatch('setUploads', { items: uploads, total: response.total })
      })
  })
}
