'use strict'

import itemsService from '../../../services/items/common'
import textService from '../../../services/items/text'
import config from '../../../config'

export default class SmallerCommand {
  constructor (item) {
    this.item = item
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const fontSize = this.item.fontSize() + config.text.resizeStep
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontSize = `${fontSize}px`
        textarea.focus()
      }
      this.item.fontSize(fontSize)
      textService.resizeTextNode(this.item)
      itemsService.refreshTools()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const fontSize = this.item.fontSize() - config.text.resizeStep
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontSize = `${fontSize}px`
        textarea.focus()
      }
      this.item.fontSize(fontSize)
      textService.resizeTextNode(this.item)
      itemsService.refreshTools()
      resolve()
    })
  }
}
