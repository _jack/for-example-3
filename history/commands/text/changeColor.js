'use strict'

import storeService from '../../../services/store'
import stageService from '../../../services/stage'

export default class ChangeColorCommand {
  constructor (item, color) {
    color = color || '#000000'
    this.item = item
    this.previousColor = item.fill()
    this.color = color
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.color = this.previousColor
        textarea.focus()
      }
      this.item.fill(this.previousColor)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const color = this.color
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.color = color
        textarea.focus()
      }
      this.item.fill(color)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }
}
