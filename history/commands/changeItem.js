'use strict'

import storeService from '../../services/store'
import stageService from '../../services/stage'
import itemsService from '../../services/items/common'

export default class ChangeItemCommand {
  constructor (itemData, startAttributes, endAttributes) {
    this.startActualSizesAndPosition = stageService.getActualSizesAndPosition()
    this.itemData = itemData
    this.startAttributes = Object.assign({}, startAttributes)
    this.endAttributes = Object.assign({}, endAttributes)
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
      const item = itemsLayer.findOne(item => item.getAttrs().objectData && item.getAttrs().objectData.id === this.itemData.id)
      item.setAttrs(this.startAttributes)

      // Scale item if stage has been scaled
      const currentActualSizesAndPosition = stageService.getActualSizesAndPosition()
      if (this.startActualSizesAndPosition.width !== currentActualSizesAndPosition.width) {
        const scale = currentActualSizesAndPosition.width / this.startActualSizesAndPosition.width
        itemsService.scaleItem(item, scale, this.startActualSizesAndPosition)
      }

      itemsService.refreshTools()
      itemsLayer.draw()

      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
      const item = itemsLayer.findOne(item => item.getAttrs().objectData && item.getAttrs().objectData.id === this.itemData.id)
      item.setAttrs(this.endAttributes)

      // Scale item if stage has been scaled
      const currentActualSizesAndPosition = stageService.getActualSizesAndPosition()
      if (this.startActualSizesAndPosition.width !== currentActualSizesAndPosition.width) {
        const scale = currentActualSizesAndPosition.width / this.startActualSizesAndPosition.width
        itemsService.scaleItem(item, scale, this.startActualSizesAndPosition)
      }

      itemsService.refreshTools()
      itemsLayer.draw()

      resolve()
    })
  }
}
