'use strict'

import axios from 'axios'
import config from '../config'

// axios.defaults.withCredentials = true
axios.defaults.baseURL = config.baseURL

export default axios
