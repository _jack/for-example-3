'use strict'

import Konva from 'konva'
import storeService from '../store'
import stageService from '../stage'

class FaceRectangle {
  static handleFaceRectangleEvents (faceRectangle) {
    faceRectangle.on('mouseenter', () => this.hover(faceRectangle))
    faceRectangle.on('touchstart', () => this.select(faceRectangle))
    faceRectangle.on('mouseleave', () => this.setDefaultState(faceRectangle))
  }

  static hover (faceRectangle) {
    if (!faceRectangle.getAttrs().isSelected) {
      const rectangles = faceRectangle.find('.faceRectangle')
      rectangles[0].stroke('#00A9F0')
      rectangles[1].stroke('#00A9F0')
      rectangles[0].opacity(1)
      rectangles[1].opacity(1)
      rectangles[2].opacity(0)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
    }
  }

  static select (faceRectangle) {
    faceRectangle.getAttrs().isSelected = true
    const rectangles = faceRectangle.find('.faceRectangle')
    rectangles[0].stroke('#00A9F0')
    rectangles[1].opacity(0)
    rectangles[2].opacity(0)
    storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
  }

  static setDefaultState (faceRectangle, force = false) {
    if (!faceRectangle.getAttrs().isSelected || force) {
      const rectangles = faceRectangle.find('.faceRectangle')
      rectangles[0].stroke('#8e8e8e')
      rectangles[1].opacity(1)
      rectangles[1].stroke('white')
      rectangles[2].opacity(1)
      rectangles[2].stroke('#8e8e8e')
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
    }
  }

  static setDefaultStateForAll () {
    const stage = storeService.get('stage')
    stage.find('.faceRectangleGroup').forEach(faceRectangleGroup => {
      this.setDefaultState(faceRectangleGroup, true)
      faceRectangleGroup.getAttrs().isSelected = false
    })
    stage.findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
  }

  /*
   * Add face rectangle
   */
  static addFaceRectangle (group) {
    const faceSizeAndCoordinates = group.getAttrs().objectData.faceRect
    const scale = group.width() / group.getAttrs().objectData.width
    const width = faceSizeAndCoordinates.width * scale
    const height = faceSizeAndCoordinates.height * scale
    const x = faceSizeAndCoordinates.x * scale
    const y = faceSizeAndCoordinates.y * scale

    // Draw three rectangles to reproduce bordered line
    const faceRectangleGroup = new Konva.Group({
      name: 'faceRectangleGroup',
      width,
      height,
      x,
      y,
      opacity: 0
    })
    let faceRectangle = new Konva.Rect({
      name: 'faceRectangle',
      width,
      height,
      stroke: '#8e8e8e',
      strokeWidth: 1,
      opacity: 0.7
    })
    faceRectangleGroup.add(faceRectangle)
    faceRectangle = new Konva.Rect({
      name: 'faceRectangle',
      width: width - 2,
      height: height - 2,
      x: 1,
      y: 1,
      stroke: 'white',
      strokeWidth: 1
    })
    faceRectangleGroup.add(faceRectangle)
    faceRectangle = new Konva.Rect({
      name: 'faceRectangle',
      width: width - 4,
      height: height - 4,
      x: 2,
      y: 2,
      stroke: '#8e8e8e',
      strokeWidth: 1,
      opacity: 0.7
    })
    faceRectangleGroup.add(faceRectangle)
    group.add(faceRectangleGroup)
    this.handleFaceRectangleEvents(faceRectangleGroup)
  }

  static showAll () {
    const stage = storeService.get('stage')
    let redraw = false
    stage.find('.faceRectangleGroup').forEach(faceRectangleGroup => {
      redraw = !faceRectangleGroup.opacity()
      faceRectangleGroup.opacity(1)
    })
    if (redraw) {
      stage.findOne(`.${stageService.ITEMS_LAYER_ID}`).batchDraw()
    }
  }

  static hideAll () {
    const stage = storeService.get('stage')
    let redraw = false
    stage.find('.faceRectangleGroup').forEach(faceRectangleGroup => {
      redraw = !faceRectangleGroup.opacity()
      faceRectangleGroup.opacity(0)
    })
    if (redraw) {
      stage.findOne(`.${stageService.ITEMS_LAYER_ID}`).batchDraw()
    }
  }

  static recalculatePositionAndSize (model) {
    const faceRectangleGroup = model.findOne('.faceRectangleGroup')
    if (faceRectangleGroup) {
      const faceSizeAndCoordinates = model.getAttrs().objectData.faceRect
      const scale = model.width() / model.getAttrs().objectData.width
      const width = faceSizeAndCoordinates.width * scale
      const height = faceSizeAndCoordinates.height * scale
      const x = faceSizeAndCoordinates.x * scale
      const y = faceSizeAndCoordinates.y * scale

      // Recalculate rectangles group
      faceRectangleGroup.setAttrs({
        width,
        height,
        x,
        y
      })

      // Recalculate inner rectangles
      const faceRectangles = faceRectangleGroup.find('.faceRectangle')
      faceRectangles[0].setAttrs({
        width,
        height
      })
      faceRectangles[1].setAttrs({
        width: width - 2,
        height: height - 2
      })
      faceRectangles[2].setAttrs({
        width: width - 4,
        height: height - 4
      })
    }
  }
}

export default FaceRectangle
