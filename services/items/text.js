import Konva from 'konva'
import storeService from '../store'
import config from '../../config'
import ChangeTextCommand from '../../history/commands/text/changeText'
import itemsService from './common'
import stageService from '../stage'

const nanoid = require('nanoid')
const TEXTAREA_OFFSET = 2

function handle (params = {}) {
  return new Promise(resolve => {
    const stage = storeService.get('stage')
    const fontSize = storeService.get('vuex').state.editor.stage.height * 0.1
    const width = stage.width()
    const height = fontSize
    const fontFamily = params.fontFamily || config.text.fonts[0].title
    const defaultParams = {
      name: 'text',
      width,
      height,
      draggable: true,
      objectData: {
        id: params.id || nanoid(),
        type: itemsService.TYPE_TEXT
      },
      fontSize,
      fontFamily: fontFamily,
      fontStyle: config.text.fonts.find(font => font.title === fontFamily).weight,
      lineHeight: 1.8,
      fill: 'black'
    }

    const textNode = new Konva.Text(Object.assign({}, defaultParams, params))
    const textGroup = new Konva.Text(Object.assign({}, defaultParams, params))
    //textGroup.add(text)

    itemsService.setNodeEvents(textNode)

    textNode
      .on('dblclick', () => {
        handleTextEditArea(textNode)
      })
      .on('beforeDeactivate', (textNode) => {
        applyText(textNode)
      })

    const layer = stage.findOne(`#${stageService.ITEMS_LAYER_ID}`)
    stageService.addToLayer(layer, textNode)
    textNode.setZIndex(storeService.get('background') ? 1 : 0)

    // Set default text and handle width/height according to the text
    if (!params.id) {
      handleText(textNode)
      textNode.width(textNode.getTextWidth())
      textNode.height(textNode.text().split('\n').length * textNode.getTextHeight() * textNode.lineHeight())
      moveToTheEmptyArea(textNode)
    }

    textNode.offsetX(textNode.width() / 2)
    textNode.offsetY(textNode.height() / 2)

    layer.draw()
    itemsService.storeStageItem(textNode)

    
    resolve(textNode)
    //resolve(textGroup)
  })
}

function moveToTheEmptyArea (textNode) {
  const stage = storeService.get('stage')
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  let y = actualSizesAndPosition.y + textNode.height() / 2
  let x = actualSizesAndPosition.x + textNode.width() / 2

  // Find empty space at the top
  let topBorder = actualSizesAndPosition.y + actualSizesAndPosition.height
  let bottomBorder = actualSizesAndPosition.y
  stage.find(node => {
    if (node.className === 'Text' && node.getAttrs().objectData.id !== textNode.getAttrs().objectData.id) {
      const nodeTopBorder = node.y() - node.height() / 2
      const nodeBottomBorder = node.y() + node.height() / 2
      if (nodeTopBorder < topBorder) {
        topBorder = nodeTopBorder
      }
      if (nodeBottomBorder > bottomBorder) {
        bottomBorder = nodeBottomBorder
      }
    }
  })

  // If there is an empty space after existing texts
  if (topBorder - actualSizesAndPosition.y < textNode.height() && actualSizesAndPosition.y + actualSizesAndPosition.height - bottomBorder > textNode.height() + 5) {
    y = bottomBorder + textNode.height() / 2 + 5
  }

  textNode.x(x)
  textNode.y(y)
}

function getDefaultText () {
  const background = storeService.get('background')
  return background && background.getAttrs().objectData.title ? background.getAttrs().objectData.title : config.text.defaultText
}

function handleText (textNode) {
  let text = textNode.text() ? textNode.text() : getDefaultText()

  // Split text if needed
  let spacesIndexs = []
  if (text.length > 20) {
    // Find spaces
    [...text].forEach((letter, index) => {
      if (letter === ' ') {
        spacesIndexs.push(index)
      }
    })

    // If spaces exists - split by the right-center one
    if (spacesIndexs.length) {
      let centerSpaceIndexes = []
      switch (spacesIndexs.length) {
        case 1:
          centerSpaceIndexes.push(spacesIndexs[0])
          break
        default:
          if (text.length > 40 && spacesIndexs.length > 1) {
            centerSpaceIndexes.push(spacesIndexs[Math.floor(spacesIndexs.length / 3)])
            centerSpaceIndexes.push(spacesIndexs[Math.floor(spacesIndexs.length / 3 * 2)])
          } else {
            centerSpaceIndexes.push(spacesIndexs[Math.floor(spacesIndexs.length / 2)])
          }
          break
      }
      centerSpaceIndexes.forEach(spaseIndex => {
        text = text.substr(0, spaseIndex) + '\n' + text.substr(spaseIndex + 1)
      })
    }
  }
  textNode.text(text.toUpperCase())

  // Set text height according to text lines amount
  resizeTextNode(textNode)
}

function resizeTextNode (textNode, textNodeHeight = null) {
  const oldY = textNode.y() - textNode.height() / 2
  textNode.height((textNodeHeight || getTextareaHeight(textNode)) + (textNode.text().split('\n').length ? 10 : 0))
  textNode.offsetY(textNode.height() / 2)
  textNode.y(oldY + textNode.height() / 2)
  storeService.get('textTools').calculatePosition()
  storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`).draw()
}

function makePng (textNode, scale = 1) {
  const stage = storeService.get('stage')
  const textLayer = new Konva.Layer()
  const textNodeClone = textNode.clone()
  const stageClone = stage.clone()
  const devicePixelRatio = window.devicePixelRatio
  stageClone.getLayers().forEach(layer => layer.hide())
  stageClone.add(textLayer)
  textLayer.add(textNodeClone)
  textNodeClone.setAttrs({
    fontSize: textNode.fontSize() * scale * devicePixelRatio,
    width: textNode.width() * scale * devicePixelRatio,
    height: textNode.height() * scale * devicePixelRatio,
    offset: {
      x: 0,
      y: 0
    },
    x: 0,
    y: 0
  })
  stageClone.width(textNodeClone.width())
  stageClone.height(textNodeClone.height())
  const base64 = stageClone.toDataURL().replace('data:image/png;base64,', '')
  stageClone.destroy()

  return base64
}

/*
 * Create textarea over canvas to change the text
 */
function handleTextEditArea (textNode) {
  const stage = storeService.get('stage')
  const textarea = createTextEditArea(textNode)
  textarea.focus()

  stage.find('Transformer').opacity(0)
  textNode.opacity(0)
  stage.find(`#${stageService.ITEMS_LAYER_ID}`).draw()
}

function applyText (textNode) {
  const stage = storeService.get('stage')
  const textarea = document.querySelector('.text-edit')

  if (textarea) {
    // Trim empty rows at the begining and end
    let emptyRowsIndexes = []
    let rows = textarea.value.split('\n')
    rows.forEach((row, index) => {
      if (row === '') {
        emptyRowsIndexes.push(index)
      } else {
        emptyRowsIndexes = []
      }
    })
    let stop = false
    rows.forEach((row, index) => {
      if (!stop) {
        if (row === '') {
          emptyRowsIndexes.push(index)
        } else {
          stop = true
        }
      }
    })
    rows = rows.filter((row, index) => !emptyRowsIndexes.includes(index))
    textarea.value = rows.join('\n')

    if (textNode.text() !== textarea.value) {
      storeService.get('commandManager').execute(new ChangeTextCommand(textNode, textarea.value))
    }
    storeService.get('$workspace').removeChild(textarea)
    stage.find('Transformer').opacity(1)
    textNode.opacity(1)
    stage.find(`#${stageService.ITEMS_LAYER_ID}`).draw()
  }
}

function createTextEditArea (textNode) {
  const setNewHeight = () => {
    textarea.style.height = textNode.getTextHeight() * textNode.lineHeight() + 'px'
    textarea.style.height = textarea.scrollHeight + 'px'
  }

  // Create textarea and style it
  let textarea = createTextarea(textNode)
  textarea.style.top = textNode.y() - textNode.height() / 2 - 2 + 'px'
  textarea.style.left = textNode.x() - textNode.width() / 2 - 1 + 'px'
  textarea.style.visibility = 'visible'
  textarea.style.height = textarea.scrollHeight + 'px'
  textarea.addEventListener('input', () => {
    // Calculate and set new height
    setNewHeight()
  })
  textarea.addEventListener('focus', () => {
    // Calculate and set new height
    setNewHeight()
  })

  return textarea
}

/*
 * Create temporary textarea to determine it's height with certain text
 */
function createTextarea (textNode, className = null) {
  const textarea = document.createElement('textarea')
  textarea.value = textNode.text()
  textarea.className = className || 'text-edit'
  textarea.style.visibility = 'hidden'
  textarea.style.width = textNode.width() + TEXTAREA_OFFSET + 'px'
  textarea.style.height = textNode.getTextHeight() * textNode.lineHeight() + 'px'
  textarea.style.fontSize = textNode.getAttrs().fontSize + 'px'
  textarea.style.lineHeight = textNode.lineHeight() * textNode.fontSize() + 'px'
  textarea.style.fontFamily = textNode.fontFamily()
  textarea.style.textAlign = textNode.align()
  textarea.style.color = textNode.getAttrs().fill
  textarea.style.fontWeight = textNode.fontStyle() === 'bold' ? 'bold' : 'nomal'
  textarea.style.fontStyle = textNode.fontStyle() === 'italic' ? 'italic' : 'none'
  textarea.style.overflow = 'hidden'
  textarea.style.margin = 0
  textarea.style.padding = 0
  storeService.get('$workspace').appendChild(textarea)

  return textarea
}

/*
 * Create temporary textarea to determine it's height with certain text
 */
function getTextareaHeight (textNode) {
  const textarea = createTextarea(textNode, 'text-edit-temp')
  const textareaHeight = textarea.scrollHeight
  textarea.remove()

  return textareaHeight
}

function handleTransformer (transformer, node) {
  transformer
    .on('transformstart', () => {
      node._tmp = {}
      node._tmp._textarea = createTextarea(node, 'text-edit-temp')
    })
    .on('transform', () => {
      // Get new height with respect of new width with help of textarea element
      let textarea = node._tmp._textarea
      textarea.style.width = node.width() + TEXTAREA_OFFSET + 'px'
      resizeTextNode(node, textarea.scrollHeight)

      // Set default offset
      node.offset({ x: 0, y: 0 })

      node.width(node.width() * node.scaleX())
      node.scaleX(1)

      // Restore offset
      node.offset({ x: node.width() / 2, y: node.height() / 2 })
    })
    .on('transformend', () => {
      node._tmp._textarea.remove()
      node.offset({ x: node.width() / 2, y: node.height() / 2 })
    })
  transformer.enabledAnchors(['middle-right', 'middle-left'])
  transformer.rotateEnabled(false)
  transformer.keepRatio(false)
  transformer.forceUpdate()
}

function handleFontRedraw (fontFamily) {
  const stage = storeService.get('stage')
  let isToRedraw = false
  stage.find(node => {
    if (node.className === 'Text' && node.fontFamily() === fontFamily) {
      isToRedraw = true
    }
  })
  if (isToRedraw) {
    // Prevent text from overflow out of the node's bounds
    stage.find(node => {
      if (node.className === 'Text' && node.fontFamily() === fontFamily) {
        const nodeText = node.text()
        node.text(nodeText + '.')
        node.text(nodeText)
      }
    })
    stage.findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
  }
}

export default {
  handle,
  resizeTextNode,
  createTextEditArea,
  makePng,
  getDefaultText,
  handleTransformer,
  handleFontRedraw
}
