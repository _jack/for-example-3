'use strict'

import storeService from '../../services/store'
import stageService from '../../services/stage'
import itemsService from '../../services/items/common'
import BaseCommand from './baseCommand'

export default class AddItemCommand extends BaseCommand {
  constructor (itemData, itemParams, activateItem = false, silent = false, restore = false, skilpOutsideHandling = true) {
    super()
    this.itemData = itemData
    this.itemParams = itemParams
    this.silent = silent
    this.restore = restore
    this.skilpOutsideHandling = skilpOutsideHandling
    this.activateItem = activateItem
    this.zIndex = null
    const oldBackground = storeService.get('background')
    if (oldBackground) {
      this.oldBackgroundObjectData = oldBackground.getAttrs().objectData
    }
  }

  execute () {
    return this.addItem(this.itemData, this.itemParams, this.restore, this.silent, this.skilpOutsideHandling)
      .then(item => {
        this.initItem(item)
      })
  }

  initItem (item) {
    this.itemAttrs = item.getAttrs()
    if (this.activateItem) {
      itemsService.activateItem(item)
    }
  }

  undo () {
    return new Promise(resolve => {
      const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
      const item = itemsLayer.findOne(item => item.getAttrs().objectData && item.getAttrs().objectData.id === this.itemData.id)
      itemsService.removeItem(item)

      // Restore old background
      if ([itemsService.TYPE_BACKGROUND, itemsService.TYPE_UPLOAD_BACKGROUND].includes(this.itemData.type) && this.oldBackgroundObjectData) {
        itemsService.handleImage(this.oldBackgroundObjectData)
      }

      resolve()
    })
  }

  redo () {
    // Delete old background if inserting a new one
    if ([itemsService.TYPE_BACKGROUND, itemsService.TYPE_UPLOAD_BACKGROUND].includes(this.itemData.type) && storeService.get('background')) {
      storeService.destroyBackground()
    }

    return this.addItem(this.itemData, this.itemParams, true, this.silent)
  }
}
