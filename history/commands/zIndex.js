'use strict'

import storeService from '../../services/store'
import stageService from '../../services/stage'

export default class ZIndexCommand {
  constructor (item, zIndex) {
    this.item = item
    this.previousZIndex = this.item.getZIndex()
    this.nextZIndex = zIndex
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      this.item.setZIndex(this.previousZIndex)
      storeService
        .get('stage')
        .findOne(`#${stageService.ITEMS_LAYER_ID}`)
        .draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      this.item.setZIndex(this.nextZIndex)
      storeService
        .get('stage')
        .findOne(`#${stageService.ITEMS_LAYER_ID}`)
        .draw()

      resolve()
    })
  }
}
