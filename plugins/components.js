import Vue from 'vue'
import leftSidebar from '../components/leftSidebar/leftSidebar.vue'
import search from '../components/leftSidebar/search.vue'
import list from '../components/leftSidebar/list.vue'
import textProperties from '../components/leftSidebar/textProperties.vue'
import popupWindow from '../components/popup/window.vue'
import landing from '../components/landing/landing.vue'
import actionContent from '../components/actionContent.vue'
import popupContent from '../components/popup/content.vue'
import userUpload from '../components/leftSidebar/userUpload.vue'
import rightSidebar from '../components/rightSidebar/rightSidebar.vue'
import actions from '../components/actions.vue'
import exportTool from '../components/rightSidebar/export.vue'
import shareTool from '../components/rightSidebar/share.vue'
import project from '../components/project.vue'
import stageSizesHeader from '../components/stageSizes/header.vue'
import stageSizesPanel from '../components/stageSizes/panel.vue'
import mobileMainMenu from '../components/mobile/mainMenu.vue'
import colorPicker from '../components/colorPicker.vue'
import dashboard from '../components/dashboard.vue'
import toolbar from '../components/toolbar.vue'
import relatedItems from '../components/relatedItems.vue'
import textTools from '../components/textTools.vue'
import selectMenu from '../components/selectMenu.vue'
import ai from '../components/ai.vue'
import modal from '../components/modal.vue'
import tools from '../components/tools'
import auth from '../components/auth'
import apiError from '../components/errors/api'
import fileError from '../components/errors/file'
import config from '../config'
import 'simplebar/dist/simplebar.css'
import VueNativeSock from 'vue-native-websocket'
import VueMasonry from 'vue-masonry-css'
import TooltipDirective from '../plugins/tooltip'

const webSocketConfig = {
  format: 'json',
  reconnection: config.webSocket.reconnection,
  reconnectionAttempts: config.webSocket.reconnectionAttempts,
  reconnectionDelay: config.webSocket.reconnectionDelay,
  connectManually: true
}

Vue.use(VueNativeSock, config.webSocket.host, webSocketConfig)
export default () => {
  Vue.use(VueMasonry)

  Vue.directive('tooltip', TooltipDirective)

  Vue.component('landing', landing)
  Vue.component('auth', auth)
  Vue.component('mobile-main-menu', mobileMainMenu)
  Vue.component('popup-window', popupWindow)
  Vue.component('popup-content', popupContent)
  Vue.component('dashboard', dashboard)
  Vue.component('actions', actions)
  Vue.component('project', project)
  Vue.component('stage-sizes-header', stageSizesHeader)
  Vue.component('stage-sizes-panel', stageSizesPanel)
  Vue.component('text-properties', textProperties)
  Vue.component('user-upload', userUpload)
  Vue.component('text-tools', textTools)
  Vue.component('left-sidebar', leftSidebar)
  Vue.component('action-content', actionContent)
  Vue.component('right-sidebar', rightSidebar)
  Vue.component('search', search)
  Vue.component('toolbar', toolbar)
  Vue.component('selectMenu', selectMenu)
  Vue.component('ai', ai)
  Vue.component('modal', modal)
  Vue.component('color-picker', colorPicker)
  Vue.component('related-items', relatedItems)
  Vue.component('list', list)
  Vue.component('tools', tools)
  Vue.component('export', exportTool)
  Vue.component('share', shareTool)
  Vue.component('api-error', apiError)
  Vue.component('file-error', fileError)
}
