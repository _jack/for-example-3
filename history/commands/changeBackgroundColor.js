'use strict'

import storeService from '../../services/store'
import backgroundService from '../../services/items/background'
import itemsService from '../../services/items/common'

export default class ChangeBackgroundColorCommand {
  constructor (color) {
    this.color = color
    this.oldColor = storeService.get('defaultBackground').fill()
    this.commandToUndo = null

    const background = storeService.get('background')
    if (background) {
      this.backgroundData = background.getAttrs().objectData
    }
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      // Handle background restore
      if (this.backgroundData) {
        backgroundService.handle(this.backgroundData)
      }

      // Change default backgorund color to start value
      const defaultBackground = storeService.get('defaultBackground')
      defaultBackground.fill(this.oldColor)
      defaultBackground.getLayer().draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      // Handle background removing
      if (this.backgroundData) {
        const background = storeService.get('stage').findOne(item => item.getAttrs().objectData && item.getAttrs().objectData.id === this.backgroundData.id)
        itemsService.removeItem(background)
      }

      // Change default backgorund color to new value
      const defaultBackground = storeService.get('defaultBackground')
      defaultBackground.fill(this.color)
      defaultBackground.getLayer().draw()

      resolve()
    })
  }
}
