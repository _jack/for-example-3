'use strict'

import storeService from '../services/store'
import dataService from '../services/data'

class CommandManager {
  constructor () {
    this.undoStack = []
    this.redoStack = []
  }

  execute (command) {
    const store = storeService.get('vuex')
    this.redoStack = []
    const promise = command.execute()
      .then(() => {
        dataService.handleCollageSaving(true)
      })
    this.undoStack.push(command)
    store.dispatch('redoStackSizeSet', this.redoStack.length)
    store.dispatch('undoStackSizeSet', this.undoStack.length)
    return promise
  }

  add (command) {
    const store = storeService.get('vuex')
    this.redoStack = []
    this.undoStack.push(command)
    store.dispatch('redoStackSizeSet', this.redoStack.length)
    store.dispatch('undoStackSizeSet', this.undoStack.length)
    dataService.handleCollageSaving(true)
  }

  undo () {
    if (!this.undoStack.length) {
      return
    }

    const lastCommand = this.undoStack.pop()
    const store = storeService.get('vuex')
    lastCommand.undo()
      .then(() => {
        dataService.handleCollageSaving(true)
      })
    this.redoStack.push(lastCommand)

    store.dispatch('redoStackSizeSet', this.redoStack.length)
    store.dispatch('undoStackSizeSet', this.undoStack.length)
  }

  redo () {
    if (!this.redoStack.length) {
      return
    }

    const lastCommand = this.redoStack.pop()
    const store = storeService.get('vuex')
    lastCommand.redo()
      .then(() => {
        dataService.handleCollageSaving(true)
      })
    this.undoStack.push(lastCommand)

    store.dispatch('redoStackSizeSet', this.redoStack.length)
    store.dispatch('undoStackSizeSet', this.undoStack.length)
  }

  getUndoStackSize () {
    return this.undoStack.length
  }

  getRedoStackSize () {
    return this.redoStack.length
  }
}

export default new CommandManager()
