'use strict'

import storeService from '../../../services/store'
import stageService from '../../../services/stage'

export default class ItalicCommand {
  constructor (item) {
    this.item = item
    this.previousFontStyle = this.item.fontStyle()
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontStyle = this.previousFontStyle
        textarea.style.fontWeight = 'normal'
        textarea.focus()
      }
      this.item.fontStyle(this.previousFontStyle)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const fontStyle = 'italic'
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.fontStyle = fontStyle
        textarea.style.fontWeight = 'normal'
        textarea.focus()
      }
      this.item.fontStyle(fontStyle)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }
}
