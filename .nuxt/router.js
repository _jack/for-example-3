import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'

const _0669a84d = () => interopDefault(import('..\\pages\\creator\\index.vue' /* webpackChunkName: "pages_creator_index" */))
const _84d9e0e2 = () => interopDefault(import('..\\pages\\creator\\dashboard.vue' /* webpackChunkName: "pages_creator_dashboard" */))
const _68a52edb = () => interopDefault(import('..\\pages\\creator\\new.vue' /* webpackChunkName: "pages_creator_new" */))
const _6a4605dd = () => interopDefault(import('..\\pages\\creator\\photo\\_id.vue' /* webpackChunkName: "pages_creator_photo__id" */))
const _4c4d690a = () => interopDefault(import('..\\pages\\creator\\template\\_id.vue' /* webpackChunkName: "pages_creator_template__id" */))

Vue.use(Router)

const scrollBehavior = function () {
      return { x: 0, y: 0 }
    }

export function createRouter() {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-exact-active',
    scrollBehavior,

    routes: [{
      path: "/creator",
      component: _0669a84d,
      name: "creator"
    }, {
      path: "/creator/dashboard",
      component: _84d9e0e2,
      name: "creator-dashboard"
    }, {
      path: "/creator/new",
      component: _68a52edb,
      name: "creator-new"
    }, {
      path: "/creator/photo/:id?",
      component: _6a4605dd,
      name: "creator-photo-id"
    }, {
      path: "/creator/template/:id?",
      component: _4c4d690a,
      name: "creator-template-id"
    }],

    fallback: false
  })
}
