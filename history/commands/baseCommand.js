'use strict'

import textService from '../../services/items/text'
import itemsService from '../../services/items/common'
import backgroundService from '../../services/items/background'

export default class BaseCommand {
  addItem (object, params = {}, restore = false, silent = false, skilpOutsideHandling = true) {
    let promise = null
    switch (object.type) {
      case itemsService.TYPE_UPLOAD_BACKGROUND:
      case itemsService.TYPE_BACKGROUND:
        promise = backgroundService.handle(object, params, restore, silent, skilpOutsideHandling)
          .then(item => {
            if (!silent) {
              itemsService.loadStageItems()
            }
            return item
          })
        break
      case itemsService.TYPE_UPLOAD:
      case itemsService.TYPE_MODEL:
      case itemsService.TYPE_OBJECT:
        promise = itemsService.handleImage(object, params, false, restore, silent, skilpOutsideHandling)
          .then(item => {
            if (!silent) {
              itemsService.loadStageItems()
            }
            return item
          })
        break
      case itemsService.TYPE_TEXT:
        if (object.fontFamily) {
          params.fontFamily = object.fontFamily
        }
        promise = textService.handle(params)
        break
    }

    return promise
  }
}
