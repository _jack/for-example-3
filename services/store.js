import commandManager from '../history/commandManager'

function init () {
  map.set('items', [])
  map.set('stage', undefined)
  map.set('activeItem', undefined)
  map.set('background', undefined)
  map.set('defaultBackground', undefined)
  map.set('$workspace', undefined)
  map.set('toolbar', undefined)
  map.set('commandManager', commandManager)
}

const map = new Map()

export default {
  get (key) {
    return map.get(key)
  },
  set (key, value) {
    return map.set(key, value)
  },
  destroyBackground () {
    map.get('background').destroy()
    map.set('background', undefined)
  },
  destroyStage () {
    map.get('stage').destroy()
    map.set('stage', undefined)
  },
  init
}
