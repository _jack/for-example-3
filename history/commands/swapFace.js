'use strict'

import modelService from '../../services/items/model'
import storeService from '../../services/store'
import stageService from '../../services/stage'

export default class SwapFaceCommand {
  constructor (item, face) {
    this.item = item
    this.face = face
    this.previousSwappedFaceData = this.item.getAttrs().objectData.currentSwappedFace
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      // If there is a current swapped face - use it, else delete current
      if (this.previousSwappedFaceData) {
        modelService.swapFace(this.item, this.previousSwappedFaceData)
          .then(() => resolve())
      } else {
        // Delete old swapped face
        this.item.find('.itemSwappedFace').destroy()
        storeService
          .get('stage')
          .findOne(`#${stageService.ITEMS_LAYER_ID}`)
          .draw()

        resolve()
      }
    })
  }

  redo () {
    return new Promise(resolve => {
      modelService.swapFace(this.item, this.face)
        .then(() => resolve())
    })
  }
}
