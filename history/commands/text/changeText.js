'use strict'

import storeService from '../../../services/store'
import stageService from '../../../services/stage'
import textService from '../../../services/items/text'

export default class ChangeTextCommand {
  constructor (item, text) {
    this.item = item
    this.text = text
    this.previousText = item.text()
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      this.item.text(this.previousText)
      textService.resizeTextNode(this.item)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      this.item.text(this.text)
      textService.resizeTextNode(this.item)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }
}
