'use strict'

import storeService from '../../services/store'
import stageService from '../../services/stage'
import itemsService from '../../services/items/common'
import BaseCommand from './baseCommand'

export default class DeleteItemCommand extends BaseCommand {
  constructor (item) {
    super()
    this.startActualSizesAndPosition = stageService.getActualSizesAndPosition()
    this.item = item
    this.zIndex = item.getZIndex()
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const itemsLayer = storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`)
      this.addItem(this.item.getAttrs().objectData, this.item.getAttrs(), true).then(item => {
        item.setZIndex(this.zIndex)

        // Scale item if stage has been scaled
        const currentActualSizesAndPosition = stageService.getActualSizesAndPosition()
        if (this.startActualSizesAndPosition.width !== currentActualSizesAndPosition.width) {
          const scale = currentActualSizesAndPosition.width / this.startActualSizesAndPosition.width
          itemsService.scaleItem(this.item, scale, this.startActualSizesAndPosition)
          this.startActualSizesAndPosition = stageService.getActualSizesAndPosition()
        }

        itemsLayer.draw()

        resolve()
      })
    })
  }

  redo () {
    return new Promise(resolve => {
      itemsService.removeItem(this.item)
      resolve()
    })
  }
}
