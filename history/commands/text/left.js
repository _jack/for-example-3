'use strict'

import storeService from '../../../services/store'
import stageService from '../../../services/stage'

export default class LeftCommand {
  constructor (item) {
    this.item = item
    this.previousAlign = this.item.align()
  }

  execute () {
    return this.redo()
  }

  undo () {
    return new Promise(resolve => {
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.textAlign = this.previousAlign
        textarea.focus()
      }
      this.item.align(this.previousAlign)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }

  redo () {
    return new Promise(resolve => {
      const textAlign = 'left'
      const textarea = document.querySelector('.text-edit')
      if (textarea) {
        textarea.style.textAlign = textAlign
        textarea.focus()
      }
      this.item.align(textAlign)
      storeService.get('stage').findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
      resolve()
    })
  }
}
