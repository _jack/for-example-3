import Konva from 'konva'
import storeService from '../store'
import itemsService from '../items/common'
import stageService from '../stage'
import objectService from './object'
import modelService from './model'
import faceRectangleService from './faceRectangle'
import backgroundService from './background'
import textService from './text'
import config from '../../config'
import photosApi from '../../api/photos'
import ChangeItemCommand from '../../history/commands/changeItem'
import { timeout } from 'q';

const TYPE_DEFAULT_BACKGROUND = 'default_background'
const TYPE_BACKGROUND = 'background'
const TYPE_MODEL = 'model'
const TYPE_OBJECT = 'object'
const TYPE_TEXT = 'text'
const TYPE_UPLOAD = 'upload'
const TYPE_UPLOAD_BACKGROUND = 'upload_background'
const FLIP_AXIS_HORIZONTAL = 'X'
const FLIP_AXIS_VERTICAL = 'Y'
const SHADOWED_TYPES = [TYPE_OBJECT, TYPE_MODEL]

const ACTION_BACKGROUNDS = 'backgrounds'
const ACTION_MODELS = 'models'
const ACTION_OBJECTS = 'objects'
const ACTION_TEXT = 'text'
const ACTION_UPLOADS = 'uploads'

function handleDraggingCoordinates (node, skipAdhesive = false, skipMoving = false) {
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  const nodeWidth = node.width() * Math.abs(node.scaleX())
  const nodeHeight = node.height() * Math.abs(node.scaleY())

  /* IMAGE ADHESIVE TO THE BORDERS */
  if (!skipAdhesive) {
    // If left corner moving to the left side
    if (
      node.x() - nodeWidth / 2 > actualSizesAndPosition.x - config.stage.borderLock &&
      node.x() - nodeWidth / 2 < actualSizesAndPosition.x + config.stage.borderLock
    ) {
      node.x(actualSizesAndPosition.x + nodeWidth / 2)
    }

    // If right corner moving to the right
    if (
      node.x() + nodeWidth / 2 > actualSizesAndPosition.x + actualSizesAndPosition.width - config.stage.borderLock &&
      node.x() + nodeWidth / 2 < actualSizesAndPosition.x + actualSizesAndPosition.width + config.stage.borderLock
    ) {
      node.x(actualSizesAndPosition.x + actualSizesAndPosition.width - nodeWidth / 2)
    }

    // If upper side moving to the top
    if (
      node.y() - nodeHeight / 2 > actualSizesAndPosition.y - config.stage.borderLock &&
      node.y() - nodeHeight / 2 < actualSizesAndPosition.y + config.stage.borderLock
    ) {
      node.y(actualSizesAndPosition.y + nodeHeight / 2)
    }

    // If bottom side moving to the bottom
    if (
      node.y() + nodeHeight / 2 > actualSizesAndPosition.y + actualSizesAndPosition.height - config.stage.borderLock &&
      node.y() + nodeHeight / 2 < actualSizesAndPosition.y + actualSizesAndPosition.height + config.stage.borderLock
    ) {
      node.y(actualSizesAndPosition.y + actualSizesAndPosition.height - nodeHeight / 2)
    }

    // If horizon line
    const backgroundPoints = backgroundService.getBackgroundPoints(node.getAttrs().objectData)
    if (backgroundPoints) {
      const background = storeService.get('background')
      const backgroundPointY = background.height() * (backgroundPoints[0].y / node.getAttrs().objectData.height) + (background.y() - background.height() / 2)
      const itemPoint = node.getAttrs().objectData.point
      if (itemPoint) {
        const itemPointY = nodeHeight * (itemPoint.y / node.getAttrs().objectData.assetHeight) + (node.y() - nodeHeight / 2)
        if (
          itemPointY > backgroundPointY - config.stage.borderLock &&
          itemPointY < backgroundPointY + config.stage.borderLock
        ) {
          node.y(backgroundPointY + (node.y() - itemPointY))
        }
      }
    }
  }

  /* PREVENT MOVING OUTSIDE THE STAGE */
  if (!skipMoving) {
    // Prevent moving node outside the stage to the left
    if (node.x() + nodeWidth / 2 < actualSizesAndPosition.x + config.stage.borderLock) {
      node.x(actualSizesAndPosition.x + config.stage.borderLock * 2 - nodeWidth / 2)
    }

    // Prevent moving node outside the stage to the right
    if (node.x() - nodeWidth / 2 > actualSizesAndPosition.x + actualSizesAndPosition.width - config.stage.borderLock) {
      node.x(actualSizesAndPosition.x + actualSizesAndPosition.width - config.stage.borderLock * 2 + nodeWidth / 2)
    }

    // Prevent moving node outside the stage to the top
    if (node.y() + nodeHeight / 2 < actualSizesAndPosition.y + config.stage.borderLock) {
      node.y(actualSizesAndPosition.y + config.stage.borderLock * 2 - nodeHeight / 2)
    }

    // Prevent moving node outside the stage to the bottom
    if (node.y() - nodeHeight / 2 > actualSizesAndPosition.y + actualSizesAndPosition.height - config.stage.borderLock) {
      node.y(actualSizesAndPosition.y + actualSizesAndPosition.height - config.stage.borderLock * 2 + nodeHeight / 2)
    }
  }
}

function handleImage (object, params = {}, activate = false, restore = false, silent = false, skipOutsideHandling = true) {
  const objectData = getObjectData(object)
  let promises = []

  // Preloading preview image
  const previewPromise = new Promise((resolve, reject) => {
    const previewImageObject = new Image()
    previewImageObject.crossOrigin = 'anonymous'
    previewImageObject.src = object.thumb
    previewImageObject.onload = () => resolve()
    previewImageObject.onerror = () => reject('Error loading preview image')
  })
  promises.push(previewPromise)

  if (objectData.currentSwappedFace) {
    // Preloading face image
    const faceImagePromise = new Promise((resolve, reject) => {
      const faceImageObject = new Image()
      faceImageObject.crossOrigin = 'anonymous'
      faceImageObject.src = objectData.currentSwappedFace.url
      faceImageObject.onload = () => resolve()
      faceImageObject.onerror = () => reject('Error loading face image')
    })
    promises.push(faceImagePromise)

    // Preloading mask image
    const faceMaskPromise = new Promise((resolve, reject) => {
      const faceMaskImageObject = new Image()
      faceMaskImageObject.crossOrigin = 'anonymous'
      faceMaskImageObject.src = objectData.maskAsset.url
      faceMaskImageObject.onload = () => resolve()
      faceMaskImageObject.onerror = () => reject('Error loading mask image')
    })
    promises.push(faceMaskPromise)
  }

  // Preloading jpg image
  const jpgImageObject = new Image()
  jpgImageObject.crossOrigin = 'anonymous'
  jpgImageObject.src = objectData.url || objectData.jpgAsset.url

  // Create an item when preview image and swapped face images (if exists) are loaded
  return Promise.all(promises)
    .then(() => createItem(objectData, params, activate, restore, silent, skipOutsideHandling))
    .catch(error => console.log(error))
}

async function createItem (objectData, params = {}, activate = false, restore = false, silent = false, skipOutsideHandling = true) {
  const stage = storeService.get('stage')
  const itemsLayer = stage.findOne(`#${stageService.ITEMS_LAYER_ID}`)
  let width, height, x, y
  switch (objectData.type) {
    case TYPE_UPLOAD:
    case TYPE_OBJECT:
      ({ width, height, x, y } = objectService.calculatePositionAndSize(objectData))
      break
    case TYPE_MODEL:
      ({ width, height, x, y } = modelService.calculatePositionAndSize(objectData))
      break
    case TYPE_UPLOAD_BACKGROUND:
    case TYPE_BACKGROUND:
      ({ width, height, x, y } = backgroundService.calculatePositionAndSize(objectData))
      break
  }
  const defaultParams = {
    width,
    height,
    x,
    y,
    draggable: true,
    objectData
  }

  if (SHADOWED_TYPES.includes(objectData.type) && objectData.shadowData) {
    defaultParams.shadowData = objectData.shadowData
  }

  // Remove sizes and coordinates from params because models have own rules
  if (objectData.type === TYPE_MODEL && !restore) {
    delete params.width
    delete params.height
    delete params.x
    delete params.y
  }

  params = Object.assign({}, defaultParams, params)
  const group = new Konva.Group(Object.assign(
    {
      name: 'image',
      opacity: silent ? 1 : 0.65
    },
    params
  ))
  group.scaleX(objectData.scaleX)
  group.scaleY(objectData.scaleY)

  // Calculate sizes of image, shadow and group if exists
  let shadowScale = 1
  let groupWidth = params.width
  let groupHeight = params.height
  if (SHADOWED_TYPES.includes(objectData.type) && objectData.shadowData && !objectData.ignoreShadow && !objectData.ignoreShadow) {
    shadowScale = params.width / objectData.width
    groupWidth = objectData.shadowData.width * shadowScale
    groupHeight = objectData.shadowData.height * shadowScale
    const widthDifference = (groupWidth - group.width()) / 2
    group.width(groupWidth)
    group.height(groupHeight)
    group.x(group.x() + widthDifference)
  }

  // Set group offsets with respect of calculated sizes
  group.offset({
    x: groupWidth / 2,
    y: groupHeight / 2
  })

  // Add image
  const previewImageObject = new Image()
  previewImageObject.crossOrigin = 'anonymous'
  previewImageObject.src = objectData.thumb
  const image = new Konva.Image({
    name: 'itemImage',
    width: params.width,
    height: params.height,
    image: previewImageObject,
    objectData: {
      id: objectData.id + '-image'
    }
  })
  group.add(image)

  // Add shadow
  if (SHADOWED_TYPES.includes(objectData.type) && objectData.shadowData) {
    const shadowImageObject = new Image()
    shadowImageObject.crossOrigin = 'anonymous'
    shadowImageObject.src = objectData.shadowData.url
    const shadow = new Konva.Image({
      name: 'itemShadow',
      width: groupWidth,
      height: groupHeight,
      image: shadowImageObject
    })
    group.add(shadow)
    shadow.moveToBottom()
    itemsLayer.draw()
  }

  setNodeEvents(group)
  if (!restore || !skipOutsideHandling) {
    handleDraggingCoordinates(group)
  }

  if (activate) {
    activateItem(group)
  }

  if (!silent) {
    startLoader(group, group.width(), group.height())
  }

  // Check for swapped faces
  modelService.handleSwappedFaces(group)

  const imageReadyCallback = (imageData) => {
    image.setImage(imageData)
    if (!silent) {
      stopLoader(group)
    }
    group.opacity(1)
    itemsLayer.draw()
  }

  // Generate masked JPG for models/objects or handle jpg for backgrounds
  if (objectData.jpgAsset) {
    Promise.all([
      getImageData(objectData.jpgAsset.url),
      getImageData(objectData.maskAsset.url)
    ])
      .then(images => mergeImages(images[0], images[1]))
      .then(imageData => {
        imageReadyCallback(imageData)
      })
  } else {
    const backgroundImage = new Image()
    backgroundImage.crossOrigin = 'anonymous'
    backgroundImage.src = objectData.url
    backgroundImage.onload = () => imageReadyCallback(backgroundImage)
  }

  // Swap face if needed
  if (objectData.currentSwappedFace) {
    await modelService.swapFace(group, objectData.currentSwappedFace)
  }

  storeStageItem(group)
  stageService.addToLayer(itemsLayer, group)

  if ([TYPE_BACKGROUND, TYPE_UPLOAD_BACKGROUND].includes(objectData.type)) {
    group.moveToBottom()
    storeService.set('background', group)
  }

  itemsLayer.draw()

  return group
}

/*
 * Load related objects
 */
function loadRelatedObjects (add = false, params = {}) {
  return new Promise(resolve => {
    const itemsTags = getItemsTagIds()
    if (!itemsTags) {
      return resolve()
    }

    const objectsParams = {
      tag_ids: itemsTags.join(',')
    }
    photosApi.objects(Object.assign(params, objectsParams))
      .then(response => {
        if (response.objects.length) {
          const objects = objectService.handlePhotos(response.objects, add, 'related')
          storeService.get('vuex').dispatch('relatedObjectsSet', {
            items: objects,
            total: response.total
          })
        }
        resolve()
      })
  })
}

/*
 * Load related models
 */
function loadRelatedModels (add = false, params = {}) {
  return new Promise(resolve => {
    const itemsTags = getItemsTagIds()
    if (!itemsTags) {
      return resolve()
    }

    const modelsParams = {
      tag_ids: itemsTags.join(',')
    }
    photosApi.models(Object.assign(params, modelsParams))
      .then(response => {
        if (response.models.length) {
          const models = modelService.handlePhotos(response.models, add, 'related')
          storeService.get('vuex').dispatch('relatedModelsSet', {
            items: models,
            total: response.total
          })
        }
        resolve()
      })
  })
}

/*
 * Load related backgrounds
 */
function loadRelatedBackgrounds (add = false, params = {}) {
  return new Promise(resolve => {
    const itemsTags = getItemsTagIds()
    if (!itemsTags) {
      return resolve()
    }

    const backgroundsParams = {
      tag_ids: itemsTags.join(',')
    }
    photosApi.backgrounds(Object.assign(params, backgroundsParams))
      .then(response => {
        if (response.backgrounds.length) {
          const backgrounds = backgroundService.handlePhotos(response.backgrounds, add, 'related')
          storeService.get('vuex').dispatch('relatedBackgroundsSet', {
            items: backgrounds,
            total: response.total
          })
        }
        resolve()
      })
  })
}

function startLoader (group, width, height) {
  // Calculate loader size
  const loaderSize = 20

  const loaderGroupParams = {
    name: 'loader',
    x: width / 2,
    y: height / 2,
    width: loaderSize,
    height: loaderSize
  }

  const loaderGroup = new Konva.Group(loaderGroupParams)
  group.add(loaderGroup)

  // Background ring
  const ring = new Konva.Ring({
    outerRadius: loaderSize,
    innerRadius: loaderSize - 5,
    fill: 'rgba(255, 255, 255, 0.5)'
  })
  loaderGroup.add(ring)

  // Loader arc
  const loader = new Konva.Arc({
    outerRadius: loaderSize,
    innerRadius: loaderSize - 5,
    angle: 60,
    fill: '#ffffff'
  })
  loaderGroup.add(loader)

  // Animate loader arc
  const angularSpeed = 90
  const animation = new Konva.Animation(function (frame) {
    var angleDiff = frame.timeDiff * angularSpeed / 1000
    loader.rotate(angleDiff)
  }, storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`))
  animation.start()
  loaderGroup.animation = animation
}

function stopLoader (group) {
  if (group.find('.loader')) {
    const loaderGroup = group.find('.loader')[0]
    if (loaderGroup && loaderGroup.animation) {
      loaderGroup.animation.stop()
      loaderGroup.destroyChildren()
      loaderGroup.destroy()
      storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`).draw()
    }
  }
}

function setNodeEvents (node) {
  const clickHandler = event => {
    const lastOpaqueNode = getLastOpaqueItem(node, event)
    if (!lastOpaqueNode) {
      storeService.get('vuex').commit('setDefaultBackgroundActive', true)
      return stageService.handleOutsideClick(false)
    }

    activateItem(lastOpaqueNode, event)
    lastOpaqueNode.startDrag()
    lastOpaqueNode._startAttributes = Object.assign({}, lastOpaqueNode.getAttrs())

    let tags = []
    if (lastOpaqueNode.getAttrs().objectData.tags) {
      lastOpaqueNode.getAttrs().objectData.tags.forEach(tag => tags.push(tag.title))
    }

    // console.log(lastOpaqueNode.getAttrs(), tags.join(', '))
    // console.log(lastOpaqueNode.getAttrs())
    // console.log(lastOpaqueNode)
  }

  let delayHoverFactor = true
 
  node
    .on('widthChange', (event) => {
      if (node.getName() === 'image') {
        node.find(node => node.className && node.className === 'Image').forEach(imageNode => {
          imageNode.width(imageNode.width() * event.newVal / event.oldVal)
        })
      }
    })
    .on('heightChange', (event) => {
      if (node.getName() === 'image') {
        node.find(node => node.className && node.className === 'Image').forEach(imageNode => {
          imageNode.height(imageNode.height() * event.newVal / event.oldVal)
        })
      }
    })
    .on('transformstart', () => {
      node._horizonLine = false
      const itemObjectData = node.getAttrs().objectData
      if (itemObjectData.point) {
        const backgroundPoints = backgroundService.getBackgroundPoints(itemObjectData)
        if (backgroundPoints) {
          const background = storeService.get('background')
          const backgroundPointY = background.height() * (backgroundPoints[0].y / node.getAttrs().objectData.height) + (background.y() - background.height() / 2)
          const nodeHeight = node.height() * node.scaleY()
          const itemPointY = nodeHeight * (itemObjectData.point.y / itemObjectData.assetHeight) + (node.y() - nodeHeight / 2)
          if (Math.round(itemPointY) === Math.round(backgroundPointY)) {
            node._horizonLine = true
          }
        }
      }
      hideTools()
    })
    .on('transform', () => {
      storeService.get('toolbar').calculatePosition()
      delayHoverFactor - false
    })
    .on('transformend', () => {
      node._endAttributes = node.getAttrs()

      // Add transform command if node has been transformed
      const previousWidth = node._startAttributes.width * node._startAttributes.scaleX
      const previousHeight = node._startAttributes.height * node._startAttributes.scaleY
      const currentWidth = node.width * node.scaleX()
      const currentHeight = node.height * node.scaleY()
      if (
        previousWidth !== currentWidth ||
        previousHeight !== currentHeight ||
        node._startAttributes.rotation !== node._endAttributes.rotation
      ) {
        storeService.get('commandManager').add(new ChangeItemCommand(node.getAttrs().objectData, node._startAttributes, node._endAttributes))
      }

      showTools()
      delayHoverFactor = true
    })
    .on('touchstart', (event) => {
      clickHandler(event)
    })
    .on('mousedown', (event) => {  
      clickHandler(event)
    })

// ----- 'Hovers through transparent' runner
  .on('mousemove', (event) => {
    if(delayHoverFactor === true){
      const lastOpaqueNode = getLastOpaqueItem(node, event)
      delayHoverFactor = false
        setTimeout(() => {
        activateJustHover(lastOpaqueNode, event)
        delayHoverFactor = true
      }, 0)
    }
  })
// ----- End of 'Hovers through transparent' runner

    .on('dragstart', () => {
      hideHelpers(true)
      if (storeService.get('vuex').state.editor.stage.showOffset) {
        stageService.createClippingMask()
      }
      delayHoverFactor = false
    })
    .on('dragmove', () => {
      handleDraggingCoordinates(node)
    })
    .on('dragend', () => {
      node._endAttributes = Object.assign({}, node.getAttrs())

      if (storeService.get('vuex').state.editor.stage.showOffset) {
        stageService.deleteClippingMask()
      }
      handleDraggingCoordinates(node)
      activateItem(node)

      // Add move command if node has been moved
      if (
        node._startAttributes.x !== node._endAttributes.x ||
        node._startAttributes.y !== node._endAttributes.y
      ) {
        storeService.get('commandManager').add(new ChangeItemCommand(node.getAttrs().objectData, node._startAttributes, node._endAttributes))
      }
      delayHoverFactor = true
    })
}

/*
 * Check out if clicked pixel is tranparent
 * and if so - activate last opaque item under current if exists
 */
function getLastOpaqueItem (item, event) {
  
  if (
    ([TYPE_OBJECT, TYPE_MODEL, TYPE_UPLOAD].includes(item.getAttrs().objectData.type))
  ) {
    const pixelData = getPixelData(item, event)

    // If transparent point
    if (
      pixelData[0] === 0 &&
      pixelData[1] === 0 &&
      pixelData[2] === 0 &&
      pixelData[3] === 0
    ) {
      // Find the item next besides the current
      const nextItem = getNextItem(item)
      return nextItem ? getLastOpaqueItem(nextItem, event) : null
    } else {
      return item
    }
  } else if (item.getAttrs().objectData.type === TYPE_TEXT) {
    // Get offsests depending on event type

    // ----- For 'Hovers through transparent in text' -> 

    const pixelData = getPixelData(item, event)

    //If transparent point
    // if (
    //   pixelData[0] === 0 &&
    //   pixelData[1] === 0 &&
    //   pixelData[2] === 0 &&
    //   pixelData[3] === 0
    // ) {
    //   // Find the item next besides the current
    //   const nextItem = getNextItem(item)
    //   return nextItem ? getLastOpaqueItem(nextItem, event) : null
    // } 

    // ----- End modifications for 'Hovers through transparent in text'

    let x, y
    if (event.type === 'touchstart') {
      const boundingBox = event.evt.target.getBoundingClientRect()
      x = event.evt.changedTouches[0].pageX - boundingBox.left
      y = event.evt.changedTouches[0].pageY - boundingBox.top
    } else {
      x = event.evt.offsetX
      y = event.evt.offsetY
    }

    // If click was inside the text
    if (
      x > item.x() - item.width() / 2 &&
      (x < item.x() + item.width() / 2) &&
      y > item.y() - item.height() / 2 &&
      (y < item.y() + item.height() / 2)
    ) {
      return item
    } else {
      const nextItem = getNextItem(item)
      return nextItem ? getLastOpaqueItem(nextItem, event) : item
    }
  } else {
    return item
  }
}

function getNextItem (item) {
  return storeService
    .get('stage')
    .findOne(`#${stageService.ITEMS_LAYER_ID}`)
    .findOne(`.${stageService.CLIPPING_GROUP_NAME}`)
    .getChildren()[item.getZIndex() - 1]
}

function getPixelData (node, event) {
  const stage = storeService.get('stage')
  const newLayer = new Konva.Layer()
  const devicePixelRatio = window.devicePixelRatio
  stage.add(newLayer)
  newLayer.moveToBottom()
  const nodeClone = node.clone()
  newLayer.add(nodeClone)
  stage.draw()

  // Calculate click/touch coordinates
  let x
  let y
  if (event.type === 'touchstart') {
    const boundingBox = event.evt.target.getBoundingClientRect()
    x = event.evt.changedTouches[0].pageX - boundingBox.left
    y = event.evt.changedTouches[0].pageY - boundingBox.top
  } else {
    x = event.evt.offsetX
    y = event.evt.offsetY
  }
  x *= devicePixelRatio
  y *= devicePixelRatio

  const pixelData = newLayer.getContext('2d').getImageData(x, y, 1, 1).data
  newLayer.destroy()

  return pixelData
}

function removeItem (item) {
  deactivateItem()
  item.destroy()
  storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`).draw()
  if ([TYPE_BACKGROUND, TYPE_UPLOAD_BACKGROUND].includes(item.getAttrs().objectData.type)) {
    storeService.set('background', null)
  }

  // Remove from items
  const filterdItems = storeService.get('items').filter(_item => {
    return _item.getAttrs().objectData.id !== item.getAttrs().objectData.id
  })
  storeService.set('items', filterdItems)

  // Refresh related items
  if (item.getAttrs().objectData.type !== TYPE_TEXT) {
    itemsService.loadStageItems()
  }

  return item
}

// ----- 'Hovers through transparent' activities

let oldNode
let effectOnce = false

function activateJustHover(node, event) {

  const activeItem = storeService.get('activeItem')

  if(node !== null){
    if(node.getAttrs().objectData.type === TYPE_TEXT ){

      // // Rect for hover
      // var rect  = new Konva.Rect({
      //   width: node.getAttrs().width,
      //   height: node.getAttrs().height,
      //   width: 200,
      //   height: 80,
      //   fill: '',
      //   fill: 'transparent',
      //   stroke: 'transparent',
      //   strokeWidth: 2
      // });
      // node.add(rect)
      // rect.on('mouseover', function() {
      //   this.stroke('#29aefb')
      // });
      // rect.on('mouseout', function() {
      //   this.stroke('transparent')
      // });

      createStrokeHover(node)

    } else {
      if (node !== oldNode || effectOnce == true){
        effectOnce = false
        createStrokeHover(node)
        // green fill on hover - tests for iteration
        // let rectFill = new Konva.Rect({
        //   x: 0,
        //   y: 0,
        //   fill: '#0f885320',
        //   width: node.getAttrs().width,
        //   height: node.getAttrs().height,
        //   cornerRadius: 10
        // });
        // node.add(rectFill) // - to add fill
      }
    }
    // Update variable for activities
    oldNode = node
    } else if (node === null){
      effectOnce = true
      const stage = storeService.get('stage')
      const activeItem = storeService.get('activeItem')
      if(!activeItem){
        stage.find('Transformer').destroy()
      }
    }
  }

  // Make strokes for all

function createStrokeHover (node) {
  const stage = storeService.get('stage')
  const activeItem = storeService.get('activeItem')
  stage.find('Transformer').destroy()
  // stage.find('Rect').destroy()
  if(activeItem){
    createTransformer(activeItem)
  }
  if (node !== activeItem){
    const transformer = new Konva.Transformer({
    node: node,
    rotateAnchorOffset: 0,
    anchorSize: 0,
    borderStroke: '#29aefb',
    borderStrokeWidth: 2
  })
  stage.find(`#${stageService.ITEMS_LAYER_ID}`)
    .add(transformer)
    .draw()
  }
}

// ----- End 'Hovers through transparent' activities

function activateItem (node, event) {
  deactivateItem()
  const store = storeService.get('vuex')
  faceRectangleService.setDefaultStateForAll()

  // Set flag that face rectangle clicked or not
  if (event) {
    const faceSelected = event.target.getAttrs().name === 'faceRectangle'
    store.dispatch('setFaceSelected', faceSelected)

    if (node.getAttrs().objectData.type === TYPE_MODEL) {
      const faceRectangle = node.findOne('.faceRectangleGroup')
      if (faceRectangle) {
        faceRectangle.getAttrs().isSelected = faceSelected
        if (faceSelected) {
          faceRectangleService.select(faceRectangle)
        } else {
          faceRectangleService.setDefaultState(faceRectangle)
        }
      }
    }
  }

  store.dispatch('elementSelected', node.getAttrs().objectData)
  storeService.set('activeItem', node)
  showTools()
  createTransformer(node)

  // Select TEXT tab if text selected
  if (node.getAttrs().objectData.type === TYPE_TEXT) {
    store.dispatch('setCurrentAction', TYPE_TEXT)
  }
}

function deactivateItem (hideTools = true) {
  const activeItem = storeService.get('activeItem')
  if (activeItem) {
    activeItem.fire('beforeDeactivate', activeItem)

    // Hide face rectangles
    if (activeItem.getAttrs().objectData.type === TYPE_MODEL) {
      //activeItem.cache();
      //activeItem.filters([Konva.Filters.Brighten]);
      //activeItem.brightness(0);
      const faceRectangle = activeItem.findOne('.faceRectangleGroup')
      if (faceRectangle) {
        faceRectangleService.setDefaultState(faceRectangle, true)
      }
    }

    const stage = storeService.get('stage')
    stage.find('Transformer').destroy()
    stage.findOne(`#${stageService.ITEMS_LAYER_ID}`).draw()
    storeService.get('vuex').dispatch('elementSelected', null)
    storeService.set('activeItem', null)
    hideHelpers(true, hideTools)
  }
}

function createTransformer (node) {
  const stage = storeService.get('stage')
  stage.find('Transformer').destroy()
  const defaultHandlers = ['top-left', 'top-right', 'bottom-left', 'bottom-right']

  const transformer = new Konva.Transformer({
    node: node,
    rotateAnchorOffset: 20,
    anchorSize: 15,
    boundBoxFunc: function (oldBoundBox, newBoundBox) {
      return handleTransforming(oldBoundBox, newBoundBox, node)
    },
    enabledAnchors: defaultHandlers,
    rotationSnaps: [0, 90, 180, 270]
  })
  transformer.on('mousedown', () => {
    node._startAttributes = Object.assign({}, node.getAttrs())
  })

  // Handle transformer behavior if node is Text
  if (node.className === 'Text') {
    textService.handleTransformer(transformer, node)
  }

  stage.find(`#${stageService.ITEMS_LAYER_ID}`)
    .add(transformer)
    .draw()
}

function handleTransforming (oldBoundBox, newBoundBox, node) {
  if (!newBoundBox.rotation) {
    const actualSizesAndPosition = stageService.getActualSizesAndPosition()

    // Set minimizing limits
    // if (newBoundBox.width < config.minimumItemWidth) {
    //   newBoundBox = oldBoundBox
    // }

    // Top side offset
    const x = node.scaleX() < 0 ? newBoundBox.x + newBoundBox.width : newBoundBox.x
    const y = node.scaleY() < 0 ? newBoundBox.y + newBoundBox.height : newBoundBox.y
    const width = newBoundBox.width * (node.scaleX() < 0 ? -1 : 1)
    const height = newBoundBox.height * (node.scaleY() < 0 ? -1 : 1)
    if (y > actualSizesAndPosition.y + actualSizesAndPosition.height - config.stage.borderLock) {
      return oldBoundBox
    }

    // Bottom side offset
    if (y + height < actualSizesAndPosition.y + config.stage.borderLock) {
      return oldBoundBox
    }

    // Left side offset
    if (x > actualSizesAndPosition.x + actualSizesAndPosition.width - config.stage.borderLock) {
      return oldBoundBox
    }

    // Right side offset
    if (x + width < actualSizesAndPosition.x + config.stage.borderLock) {
      return oldBoundBox
    }
  }

  // Smart scale with respect of horizon line
  if (node._horizonLine) {
    const itemObjectData = node.getAttrs().objectData
    let xRatio, yRatio
    const xMultilplire = oldBoundBox.x !== newBoundBox.x ? -1 : 1
    const yMultilplire = oldBoundBox.y !== newBoundBox.y ? -1 : 1
    xRatio = itemObjectData.point.x / itemObjectData.assetWidth
    yRatio = itemObjectData.point.y / itemObjectData.assetHeight
    xRatio = oldBoundBox.x === newBoundBox.x ? xRatio : 1 - xRatio
    yRatio = oldBoundBox.y === newBoundBox.y ? yRatio : 1 - yRatio
    const xOffset = xRatio * (oldBoundBox.width - newBoundBox.width)
    const yOffset = yRatio * (oldBoundBox.height - newBoundBox.height)
    newBoundBox.x = newBoundBox.x + xOffset * xMultilplire
    newBoundBox.y = newBoundBox.y + yOffset * yMultilplire
  }

  return newBoundBox
}

function getObjectData (object) {
  return {
    id: object.id,
    type: object.type,
    modelPoints: object.modelPoints,
    objectPoints: object.objectPoints,
    point: object.point,
    assetWidth: object.assetWidth,
    assetHeight: object.assetHeight,
    jpgAsset: object.jpgAsset,
    maskAsset: object.maskAsset,
    width: object.width,
    height: object.height,
    scaleX: object.horizontalMirror ? -1 : 1,
    scaleY: object.verticalMirror ? -1 : 1,
    url: object.url,
    photoId: object.photoId,
    thumb: object.thumb,
    title: object.title,
    ignoreShadow: object.ignoreShadow,
    shadowData: object.shadowData,
    tags: object.tags,
    tagScore: object.tagScore,
    textScore: object.textScore,
    croppedWithAI: object.croppedWithAI,
    faceRect: object.faceRect,
    face: object.face,
    currentSwappedFace: object.swappedFace,
    loadingStatuses: {
      previewLoaded: false,
      imageLoaded: false,
      swappedFaceLoaded: false
    },
    swappedFacesData: {
      faces: [],
      total: 0,
      page: 0
    }
  }
}

function getMainAttributes (attributes) {
  let resultAttributes = {}
  let mainAttributes = ['width', 'height', 'x', 'y', 'draggable', 'rotation', 'scaleX', 'scaleY', 'zIndex']

  if (attributes.type === TYPE_TEXT) {
    mainAttributes = [...mainAttributes, ...['offsetX', 'offsetY', 'text', 'fontStyle', 'fontSize', 'fontFamily', 'fill', 'height', 'align']]
  }

  mainAttributes.forEach(attribute => {
    if (attributes[attribute]) {
      resultAttributes[attribute] = attributes[attribute]
    }
  })

  return resultAttributes
}

function showTools () {
  const activeItem = storeService.get('activeItem')
  if (activeItem) {
    if (activeItem.getAttrs().objectData.type !== TYPE_TEXT) {
      storeService.get('toolbar').show()
      storeService.get('ai').show()
      storeService.get('textTools').hide()
    } else {
      storeService.get('textTools').show()
      storeService.get('toolbar').show()
    }
  }
}

function hideTools (hideTextTools = true) {
  storeService.get('toolbar').hide()
  storeService.get('ai').hide()

  if (hideTextTools) {
    storeService.get('textTools').hide()
  }
}

function refreshTools () {
  const activeItem = storeService.get('activeItem')
  if (activeItem) {
    storeService.get('toolbar').calculatePosition()
    storeService.get('textTools').calculatePosition()
    storeService.get('ai').calculatePosition()
  }
}

function hideHelpers (forceRedraw = false, hideTextTools = true) {
  storeService.get('stage').find('Transformer').destroy()
  hideTools(hideTextTools)

  if (forceRedraw) {
    storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`).draw()
  }
}

function showHelpers (node, forceRedraw = false) {
  createTransformer(node)
  showTools()
  if (forceRedraw) {
    storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`).draw()
  }
}

function getPreviousPositoinRatio (item, previousSizesAndPosition) {
  // Previous x position ratio
  const previousXRatio = (item.x() - previousSizesAndPosition.x) / previousSizesAndPosition.width

  // Previous y position ratio
  const previousYRatio = (item.y() - previousSizesAndPosition.y) / previousSizesAndPosition.height

  return { previousXRatio, previousYRatio }
}

function recalculateItems (previousSizesAndPosition) {
  const stage = storeService.get('stage')

  // Recalculate background group sizes and position
  const backgroundGroup = stage
    .findOne(`#${stageService.ITEMS_LAYER_ID}`)
    .findOne(node => node.getAttrs().objectData && [TYPE_BACKGROUND, TYPE_UPLOAD_BACKGROUND].includes(node.getAttrs().objectData.type))
  if (backgroundGroup) {
    const backgroundPositionAndSizes = backgroundService.calculatePositionAndSize(backgroundGroup.getAttrs().objectData)
    backgroundGroup.offsetX(backgroundPositionAndSizes.width / 2)
    backgroundGroup.offsetY(backgroundPositionAndSizes.height / 2)
    backgroundGroup.x(backgroundPositionAndSizes.x)
    backgroundGroup.y(backgroundPositionAndSizes.y)
    backgroundGroup.width(backgroundPositionAndSizes.width)
    backgroundGroup.height(backgroundPositionAndSizes.height)
  }

  // Recalculate models' and objects' sizes and position
  stage
    .find(node => node.getAttrs().objectData && [TYPE_MODEL, TYPE_OBJECT, TYPE_TEXT].includes(node.getAttrs().objectData.type))
    .forEach(item => {
      recalculateItem(item, previousSizesAndPosition)
    })
}

function recalculateItem (item, previousSizesAndPosition) {
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  const itemWidth = item.width()
  const itemHeight = item.height()
  let previousXRatio, previousYRatio
  ({ previousXRatio, previousYRatio } = getPreviousPositoinRatio(item, previousSizesAndPosition))
  item.x(actualSizesAndPosition.x + actualSizesAndPosition.width * previousXRatio)
  item.y(actualSizesAndPosition.y + actualSizesAndPosition.height * previousYRatio)

  if (item.getAttrs().objectData.type !== TYPE_TEXT) {
    const scale = actualSizesAndPosition.height / previousSizesAndPosition.height
    item.width(itemWidth * scale)
    item.height(itemHeight * scale)
    item.offsetX(item.width() / 2)
    item.offsetY(item.height() / 2)
  }

  // If model - handle face rectangle and face itself
  if (item.getAttrs().objectData.type === TYPE_MODEL) {
    faceRectangleService.recalculatePositionAndSize(item)
    modelService.recalculateSwappedFacePositionAndSize(item)
  }

  // Check if item's right side is outside the left rim somehow
  if (item.x() + item.width() / 2 < actualSizesAndPosition.x + config.stage.borderLock) {
    item.x(actualSizesAndPosition.x + config.stage.borderLock * 2 - item.width() / 2)
  }

  // Check if item's left side is outside the right rim somehow
  if (item.x() - item.width() / 2 > actualSizesAndPosition.x + actualSizesAndPosition.width - config.stage.borderLock) {
    item.x(actualSizesAndPosition.x + actualSizesAndPosition.width - config.stage.borderLock * 2 + item.width() / 2)
  }
}

function scaleItem (item, scale, previousSizesAndPosition) {
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  const previousYRatio = (item.y() - previousSizesAndPosition.y) / previousSizesAndPosition.height
  const newWidth = Math.abs(item.width() * scale)
  const newHeight = Math.abs(item.height() * scale)
  item.width(newWidth)
  item.height(newHeight)
  item.offsetX(newWidth / 2)
  item.offsetY(newHeight / 2)
  item.x((item.x() - previousSizesAndPosition.x) * Math.abs(scale) + actualSizesAndPosition.x)
  item.y(actualSizesAndPosition.y + actualSizesAndPosition.height * previousYRatio)

  // If model - handle face rectangle and face itself
  if (item.getAttrs().objectData.type === TYPE_MODEL) {
    faceRectangleService.recalculatePositionAndSize(item)
    modelService.recalculateSwappedFacePositionAndSize(item)
  }

  // If text - handle text size etc.
  if (item.getAttrs().objectData.type === TYPE_TEXT) {
    item.fontSize(item.fontSize() * Math.abs(scale))
  }
}

// Calculate sizes and positions of clicked related item
function calculateRelated (currentItem, relatedItem) {
  const heightRatio = Math.abs(currentItem.height() * currentItem.scaleY()) / relatedItem.height
  let width = Math.abs(relatedItem.width * heightRatio)
  let height = Math.abs(relatedItem.height * heightRatio)
  let x = currentItem.x()
  let y = currentItem.y()

  return { width, height, x, y }
}

// TODO: remove unused attributes for TYPE_UPLOAD
function handleAssetObject (type, assetObject) {
  let result = {
    id: assetObject.id,
    type
  }
  result.thumb = assetObject.thumb1x.url
  result.photoId = assetObject.photoId
  result.tags = assetObject.tags
  result.tagScore = assetObject.tagScore
  result.textScore = assetObject.textScore
  result.horizontalMirror = assetObject.horizontalMirror
  result.verticalMirror = assetObject.verticalMirror

  switch (type) {
    case TYPE_MODEL:
    case TYPE_OBJECT:
    case TYPE_UPLOAD:
      result.url = assetObject.editorAsset.url
      result.width = assetObject.editorAsset.width
      result.height = assetObject.editorAsset.height
      result.point = assetObject.point
      result.assetWidth = assetObject.asset.width
      result.assetHeight = assetObject.asset.height
      result.jpgAsset = assetObject.editorJpgAsset
      result.maskAsset = assetObject.editorMaskAsset
      result.croppedWithAI = assetObject.croppedWithAI
      result.faceRect = assetObject.faceRect
      result.face = assetObject.face
      result.swappedFace = assetObject.swappedFace
      break
    case TYPE_UPLOAD_BACKGROUND:
    case TYPE_BACKGROUND:
      result.url = assetObject.asset.url
      result.width = assetObject.asset.width
      result.height = assetObject.asset.height
      result.modelPoints = assetObject.modelPoints
      result.objectPoints = assetObject.objectPoints
      break
  }

  if (SHADOWED_TYPES.includes(type)) {
    result.shadowData = assetObject.editorShadowAsset
    result.ignoreShadow = assetObject.ignoreShadow
  }

  return result
}

function sortItems (items) {
  return items.sort((previousItem, nextItem) => {
    const stageRatio = storeService.get('vuex').state.editor.stage.realWidth / storeService.get('vuex').state.editor.stage.realHeight

    if (stageRatio > 1) {
      return previousItem.height / previousItem.width - nextItem.height / nextItem.width
    } else {
      return nextItem.height / nextItem.width - previousItem.height / previousItem.width
    }
  })
}

function getItemsTagIds () {
  let tags = {}

  storeService.get('items').forEach(item => {
    const itemTags = item.getAttrs().objectData.tags
    if (itemTags && itemTags.length) {
      itemTags.slice(0, 5).forEach(tag => {
        tags[tag.id] = null
      })
    }
  })

  return Object.keys(tags)
}

function search (params) {
  return new Promise(resolve => {
    const store = storeService.get('vuex')
    if (!params.query) {
      return resolve()
    }

    let promises = []

    // Handle backgrounds
    promises.push(photosApi.backgrounds(Object.assign(backgroundService.getSortingParams(backgroundService.SORT_RISING), params))
      .then(backgroundsData => {
        let backgrounds = []
        backgroundsData.backgrounds.forEach(assetObject => {
          const background = handleAssetObject(TYPE_BACKGROUND, assetObject)
          backgrounds.push(background)
        })
        store.dispatch('setSearchBackgrounds', { items: backgrounds, total: backgroundsData.total, page: 1 })
      }))

    // Handle objects
    promises.push(photosApi.objects(Object.assign(objectService.getSortingParams(objectService.SORT_RISING), params))
      .then(objectsData => {
        let objects = []
        objectsData.objects.forEach(assetObject => {
          const object = handleAssetObject(TYPE_OBJECT, assetObject)
          objects.push(object)
        })
        store.dispatch('setSearchObjects', { items: objects, total: objectsData.total, page: 1 })
      }))

    // Handle models
    promises.push(photosApi.models(Object.assign(modelService.getSortingParams(modelService.SORT_RISING), params))
      .then(modelsData => {
        let models = []
        modelsData.models.forEach(assetObject => {
          const model = handleAssetObject(TYPE_MODEL, assetObject)
          models.push(model)
        })
        store.dispatch('setSearchModels', { items: models, total: modelsData.total, page: 1 })
      }))

    Promise.all(promises)
      .then(() => resolve())
  })
}

function handleItemsByIds (ids, type) {
  let promises = []
  let itemsData = []
  ids.slice(0, 5).forEach(elementId => {
    const promise = photosApi.assetData(type, elementId)
      .then(response => {
        itemsData.push(response.data)
      })
    promises.push(promise)
  })

  Promise.all(promises)
    .then(() => {
      spreadItems(itemsData, type)
    })
}

function spreadItems (itemsData, type) {
  const PADDING = 35
  const stage = storeService.get('stage')
  const actualSizesAndPosition = stageService.getActualSizesAndPosition()
  const itemsLayer = stage.findOne(`#${stageService.ITEMS_LAYER_ID}`)

  let itemsCalculatedData = {
    widthsSum: 0,
    itemsData: [],
    itemsCalculatedData: []
  }

  // Calculate widths sum
  itemsData.forEach(itemData => {
    let itemCalculatedData
    itemData = handleAssetObject(type, itemData)
    if (type === TYPE_MODEL) {
      itemCalculatedData = modelService.calculatePositionAndSize(itemData)
    } else if (type === TYPE_OBJECT) {
      itemCalculatedData = objectService.calculatePositionAndSize(itemData)
    }
    itemsCalculatedData.itemsData.push(itemData)
    itemsCalculatedData.itemsCalculatedData.push(itemCalculatedData)
    itemsCalculatedData.widthsSum += itemCalculatedData.width + PADDING
  })

  itemsData.forEach((itemData, index) => {
    let params = {}
    let restore = false
    let itemHeight = itemsCalculatedData.itemsCalculatedData[index].height

    // If items one by one don't fit to the stage width - resize it to fit the stage width one by one
    if (itemsCalculatedData.widthsSum > storeService.get('vuex').state.editor.stage.width) {
      restore = true
      const itemWidth = (itemsCalculatedData.itemsCalculatedData[index].width - PADDING) / itemsCalculatedData.widthsSum * storeService.get('vuex').state.editor.stage.width
      itemHeight = itemData.editorAsset.height * (itemWidth / itemsCalculatedData.itemsData[index].width)
      params.width = itemWidth
      params.height = itemHeight
      itemsCalculatedData.itemsCalculatedData[index].width = itemWidth
      itemsCalculatedData.itemsCalculatedData[index].height = itemHeight
    }

    params.y = actualSizesAndPosition.y + actualSizesAndPosition.height - itemHeight / 2

    handleImage(
      itemsCalculatedData.itemsData[index],
      params,
      false,
      restore
    )
      .then(item => {
        const itemWidth = itemsCalculatedData.itemsCalculatedData[index].width

        // Find last item
        const mountedItems = itemsLayer.find(node => {
          return node.getAttrs().objectData && node.nodeType === 'Group' &&
              node.getAttrs().objectData.id !== itemData.id &&
              node.getAttrs().objectData.type === type
        })
        const lastMountedItem = mountedItems[type === TYPE_OBJECT ? mountedItems.length - 1 : 0]

        // Set x next to the previous item
        if (!lastMountedItem) {
          if (itemsCalculatedData.widthsSum > storeService.get('vuex').state.editor.stage.width) {
            item.x(actualSizesAndPosition.x + itemWidth / 2)
          } else {
            item.x((storeService.get('vuex').state.editor.stage.width - itemsCalculatedData.widthsSum) / 2 + itemWidth / 2 + actualSizesAndPosition.x)
          }
        } else {
          item.x(lastMountedItem.x() + lastMountedItem.width() / 2 + itemWidth / 2)
        }
        item.x(item.x() + PADDING)

        if (type === TYPE_MODEL) {
          item.moveToBottom()
        } else if (type === TYPE_OBJECT) {
          item.moveToTop()
        }
      })
  })
}

function storeStageItem (item) {
  storeService.get('items').push(item)
}

function getImageData (imageOrUrl, x = 0, y = 0, width = 0, height = 0) {
  return new Promise((resolve, reject) => {
    const callback = image => {
      const canvas = document.createElement('canvas')
      const context = canvas.getContext('2d')
      canvas.width = image.width
      canvas.height = image.height
      context.drawImage(image, 0, 0, image.width, image.height)
      return context.getImageData(x, y, width || image.width, height || image.height)
    }

    if (typeof imageOrUrl === 'string' || imageOrUrl instanceof String) {
      const img = new Image()
      img.crossOrigin = 'Anonymous'
      img.onload = function () {
        resolve(callback(img))
      }
      img.onerror = () => reject()
      img.src = imageOrUrl
    } else {
      resolve(callback(imageOrUrl))
    }
  })
}

function mergeImages (image1, image2) {
  return new Promise(resolve => {
    // Apply mask's alpha to jpg
    for (let i = 0; i < image1.data.length; i += 4) {
      // Set alpha layer
      image1.data[i + 3] = image2.data[i]
    }

    // Make canvas with masked jpg
    const canvas = document.createElement('canvas')
    canvas.width = image1.width
    canvas.height = image1.height
    canvas.getContext('2d').putImageData(image1, 0, 0)

    // Make png from masked jpg canvas
    const imageData = canvas.toDataURL()
    const image = new Image()
    image.src = imageData

    resolve(image)
  })
}

function loadStageItems () {
  // Clear existent items
  const store = storeService.get('vuex')
  store.dispatch('relatedObjectsSet', { items: [], total: 0, page: 0 })
  store.dispatch('relatedModelsSet', { items: [], total: 0, page: 0 })
  store.dispatch('relatedBackgroundsSet', { items: [], total: 0, page: 0 })
  store.dispatch('setRelatedToStageItemsLoading')

  // Collect promises if there is some tags
  const itemsTags = getItemsTagIds()
  const params = { per_page: 50 }
  let promises = []
  if (itemsTags.length) {
    promises.push(loadRelatedObjects(false, params))
    promises.push(loadRelatedModels(false, params))
    promises.push(loadRelatedBackgrounds(false, params))
  }

  // Start promises
  Promise
    .all(promises)
    .then(() => {
      let itemsQuantity = 20

      // Shuffle models, objects, backgrounds, etc...
      let items = []
      const relatedModels = store.state.editor.related.models.items
      const relatedObjects = store.state.editor.related.objects.items
      const relatedBackgrounds = store.state.editor.related.backgrounds.items
      let itemType = TYPE_MODEL
      for (let i = 0; i < itemsQuantity; i++) {
        if (itemType === TYPE_MODEL) {
          if (relatedModels.length && relatedModels[i]) {
            items.push(relatedModels[i])
          }
          itemType = TYPE_OBJECT
        } else if (itemType === TYPE_OBJECT) {
          if (relatedBackgrounds.length && relatedBackgrounds[i]) {
            items.push(relatedBackgrounds[i])
          }
          itemType = TYPE_BACKGROUND
        } else if (itemType === TYPE_BACKGROUND) {
          if (relatedObjects.length && relatedObjects[i]) {
            items.push(relatedObjects[i])
          }
          itemType = TYPE_MODEL
        }
      }

      store.dispatch('setRelatedToStageItems', items.slice(0, itemsQuantity))
      store.dispatch('setRelatedToStageItemsLoaded')
    })
}

function hideShadow (item) {
  return new Promise(resolve => {
    const activeItemImage = item.findOne('.itemImage')
    const activeItemImageWidth = activeItemImage.width()
    const activeItemImageHeight = activeItemImage.height()
    const shadow = item.findOne('.itemShadow')

    // Scale item's group as it's no shadow now
    const widthDifference = item.width() * item.scaleX() - activeItemImageWidth * item.scaleX()
    const heightDifference = item.height() * item.scaleX() - activeItemImageHeight * item.scaleX()
    item.width(activeItemImageWidth)
    item.height(activeItemImageHeight)
    activeItemImage.width(activeItemImageWidth)
    activeItemImage.height(activeItemImageHeight)
    item.offsetX(item.width() / 2)
    item.offsetY(item.height() / 2)
    item.x(item.x() - widthDifference / 2)
    item.y(item.y() - heightDifference / 2)

    // Hide shadow
    shadow.opacity(0)
    storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`).draw()

    resolve()
  })
}

function revealShadow (item) {
  return new Promise(resolve => {
    const activeItemImage = item.findOne('.itemImage')
    const activeItemImageWidth = activeItemImage.width()
    const activeItemImageHeight = activeItemImage.height()
    const shadow = item.findOne('.itemShadow')

    // Scale item's group as it's have shadow now
    const shadowScale = activeItemImageWidth / item.getAttrs().objectData.width
    const shadowWidth = item.getAttrs().objectData.shadowData.width * shadowScale
    const shadowHeight = item.getAttrs().objectData.shadowData.height * shadowScale
    if (shadowWidth > item.width()) {
      const widthDifference = item.width() * item.scaleX() - shadowWidth * item.scaleX()
      item.width(shadowWidth)
      activeItemImage.width(activeItemImageWidth)
      item.offsetX(item.width() / 2)
      item.x((item.x() - widthDifference / 2))
    }
    if (shadowHeight > item.height()) {
      const heightDifference = item.height() * item.scaleX() - shadowHeight * item.scaleX()
      item.height(shadowHeight)
      activeItemImage.height(activeItemImageHeight)
      item.offsetY(item.height() / 2)
      item.y(item.y() - heightDifference / 2)
    }

    // Reveal shadow
    shadow.opacity(1)
    shadow.width(shadowWidth)
    shadow.height(shadowHeight)
    storeService.get('stage').find(`#${stageService.ITEMS_LAYER_ID}`).draw()

    resolve()
  })
}

function updateObjectData (item, objectData) {
  if (storeService.get('activeItem')) {
    storeService.get('vuex').dispatch('elementSelected', objectData)
  }
  item.getAttrs().objectData = objectData
}

export default {
  updateObjectData,
  handleItemsByIds,
  spreadItems,
  loadStageItems,
  storeStageItem,
  loadRelatedModels,
  loadRelatedObjects,
  loadRelatedBackgrounds,
  search,
  getItemsTagIds,
  sortItems,
  handleAssetObject,
  activateItem,
  deactivateItem,
  removeItem,
  handleDraggingCoordinates,
  calculateRelated,
  refreshTools,
  handleImage,
  getMainAttributes,
  showHelpers,
  setNodeEvents,
  hideHelpers,
  recalculateItems,
  scaleItem,
  getImageData,
  hideShadow,
  mergeImages,
  revealShadow,
  TYPE_BACKGROUND,
  TYPE_DEFAULT_BACKGROUND,
  TYPE_MODEL,
  TYPE_OBJECT,
  TYPE_TEXT,
  TYPE_UPLOAD,
  TYPE_UPLOAD_BACKGROUND,
  FLIP_AXIS_HORIZONTAL,
  FLIP_AXIS_VERTICAL,
  SHADOWED_TYPES,
  ACTION_BACKGROUNDS,
  ACTION_MODELS,
  ACTION_OBJECTS,
  ACTION_TEXT,
  ACTION_UPLOADS
}
