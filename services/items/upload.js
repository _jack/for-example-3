import itemsService from './common'

function handlePhotos (assetObjects) {
  let objects = []

  assetObjects.forEach(assetObject => {
    const type = assetObject.upload_type === 'background' ? itemsService.TYPE_UPLOAD_BACKGROUND : itemsService.TYPE_UPLOAD
    const object = itemsService.handleAssetObject(type, assetObject)
    objects.push(object)
  })

  return objects
}

export default {
  handlePhotos
}
